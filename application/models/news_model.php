<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_model extends CI_Model {

	/*public function tambahData($judul_berita,$kategori_berita,$isi_berita,$video_berita,$date,$userfile){
		$data = array(
			'judul_berita' 		=> $judul_berita,
			'kategori_berita'	=> $kategori_berita,
			'isi_berita' 		=> $isi_berita,
			'video_berita'		=> $video_berita,
			'date'				=> $date,
			);
		$this->db->insert('news',$data);
	}

	public function all() {
		//query semua record di table products
		$data = $this->db->get('products');
		return $data->result_array();
	}*/

	/*public function tambahGambar($image1,$image2,$image3,$image4,$image5){
		$data = array(
			'image1' 		=> $image1,
			'image2'		=> $image2,
			'image3' 		=> $image3,
			'image4'		=> $image4,
			'image5'		=> $image5
			);
		$this->db->insert('images',$data);
	}*/

	public function inputberita($judul_berita,$kategori_berita,$isi_berita,$video_berita,$date){
		$data = array(
			'judul_berita' 		=> $judul_berita,
			'kategori_berita'	=> $kategori_berita,
			'isi_berita' 		=> $isi_berita,
			'video_berita'		=> $video_berita,
			'date'				=> $date,
			);
		$this->db->insert('news',$data);
	}

	public function updateberita($id_berita,$judul_berita,$kategori_berita,$isi_berita,$video_berita,$date){
		$data = array(
			'judul_berita' 		=> $judul_berita,
			'kategori_berita'	=> $kategori_berita,
			'isi_berita' 		=> $isi_berita,
			'video_berita'		=> $video_berita,
			'date'				=> $date,
			);
		$this->db->where('id_berita',$id_berita);
		$this->db->update('news',$data);
	}

	public function getidberita($judul_berita, $date){
		$this->db->order_by('id_berita', 'DESC');
		$query = $this->db->get_where('news',array(
			'judul_berita' 		=> $judul_berita,			
			'date' 				=> $date
			));
		return $query->row();
	}

	public function masukkanfile($namafile,$idberita){
		$this->db->insert('images',array('id_berita' => $idberita, 'foto_berita' => $namafile));
	}

	public function allnews() {
		//query semua record di table products
		$data = $this->db->get('news');
		return $data->result_array();
	}

	public function allimages() {
		//query semua record di table products
		$data = $this->db->get('images');
		return $data->result_array();
	}

	public function findnews() {
		/*$query = $this->db->get_where('news',array('id_berita' => $id_berita));
		return $query->row();*/

		$query = $this->db->get('news');
		return $query->row();

		//Query mencari record berdasarkan id
		// $data = $this->db->get('news');
		// return $data->row();

		// $query = $this->db->get_where('news',array('id_berita' => $id_berita));
		// return $query->row();
		
		// $query = $this->db->get();
		// $ret = $query->row();
		// return $ret->judul_berita;

		// $row = $this->db->get_where('news', array('id_berita' => $id_berita))->row();

		/*$price = $this->db->select('judul_berita')
                  ->get_where('news', array('judul_berita' => $judul_berita))
                  ->row()
                  ->judul_berita;*/

		/*$this->db->select('*');
		$this->db->from('news');
		$this->db->where('id_berita', $id_berita );
		$query = $this->db->get();

	    if ( $query->num_rows() > 0 )
	    {
	        $row = $query->row_array();
	        return $row;
	    }*/

	}

	public function findimages($id_berita) {
		//Query mencari record berdasarkan id		
		/*$hasil = $this->db->where('id_berita', $id_berita)
						  ->limit(1)
						  ->get('images', 'foto_berita');
		if($hasil->num_rows() > 0){
			return $hasil->row();
		} else {
			return array();
		}*/
		$query = $this->db->get_where('images',array('id_berita' => $id_berita));
		return $query->row();
	}

	/*public function products($id_berita) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('products.id', $id);
        $query = $this->db->get();
        return $query->row();
	}*/	

	public function deleteberita($id_berita){
		$this->db->delete('news',array('id_berita' => $id_berita));
		//unlink("upload/".$id_berita);
		$this->db->delete('images',array('id_berita' => $id_berita));
	}

	public function getberita($id_berita){
		$query = $this->db->get_where('news',array('id_berita' => $id_berita));
		return $query->row();
	}

	public function getBeritaAll(){
		$this->db->from('news');
		$this->db->join('images','images.id_berita=news.id_berita');
		$query = $this->db->get();
		return $query->result();
	}


}
