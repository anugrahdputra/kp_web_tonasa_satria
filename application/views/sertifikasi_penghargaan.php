<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Header-->
<?php $this->load->view('layout/header') ?>
                
<!--End Header-->
        
        <section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>Date</span></h4>
                            </div>
                            <ul class="datepicker">
                                <li>
   
                                </li>
                            </ul>
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Sertifikasi</span></h4>
                            </div>
                                <!-- <img class="left_img img-thumbnail" src="<?php echo base_url(); ?>assets/images/pabrikok.jpg"> -->
                                <div><p> <b>Sertifikasi Produk Penggunaan Tanda SNI (SPPT-SNI)</b><br/>
                                Sebagai perseroan yang menghasilkan produk semen, perseroan memiliki standar yang memenuhi ketentuan yang ditetapkan oleh Pemerintah RI, yaitu SNI. Semen Portland jenis I sesuai dengan SNI-15-2049-2004, Semen Portland (PCC) sesuai dengan SNI-15-0302-2004. Untuk pengawasan SPPT-SNI ini dilakukan oleh Lembaga Sertifikasi Produk (LS-Pro) Departemen Perindustrian RI setiap tahun sekali. <br/></p>
                                <p></p></div>
                                <div><img class="left_img img-thumbnail" src="<?php echo base_url(); ?>assets/images/ISO 9001.jpg" alt="ISO9001" />
                                <p> <b>Sistem Manjemen Mutu - SMM (QMS ISO 9001:2008)</b><br/>
                                Sebagai perseroan yang menghasilkan produk semen, perseroan memiliki standar yang memenuhi ketentuan yang ditetapkan oleh Pemerintah RI, yaitu SNI. Semen Portland jenis I sesuai dengan SNI-15-2049-2004, Semen Portland (PCC) sesuai dengan SNI-15-0302-2004. Untuk pengawasan SPPT-SNI ini dilakukan oleh Lembaga Sertifikasi Produk (LS-Pro) Departemen Perindustrian RI setiap tahun sekali. <br/></p>
                                <p></p></div>
                                <div><img class="left_img img-thumbnail" src="<?php echo base_url(); ?>assets/images/ISO 14001.jpg" alt="ISO 14001" />
                                <p> <b>Sistem Manajemen Lingkungan - SML (EMS ISO 14001:2004)</b><br>
                                SISO 14001 Sudah menjadi komitmen perseroan yaitu “menjadi produsen semen yang ramah lingkungan”. Berbagai program dan kegiatan yang dilakukan, antara lain meminimasi dampak negative dari kegiatan operasional perseroan, meliputi pelaksanaan program efisiensi pemakaian sumber daya alam dan energy, melaksanakan kegiatan konservasi lahan bekas tambang, membina hubungan harmonis dengan masyarakat sekitar melalui PKBL-Program kemitraan dan Bina lingkungan serta CSR- Corporate Social Responsibility. Kepedulian Perseroan lebih lanjut terhadap pengelolaan lingkungan adalah menangani limbah industry baik internal maupun eksternal dengan memanfaatkannya dalam proses produksi. Keikutsertaan dalam penilaian peringkat kinerja perseroan (PROPER) dalam bidang lingkungan juga telah dilakukan,dan tahun ini perseroan sedang dalam penjajakan menuju PROPER Hijau. Untuk SML ini, sejak tahun 2000 Perseroan telah resmi mengimplementasikan persyratan ISO 14001 dan memperoleh sertifikat. Perseroan diaudit oleh PT. Sucofindo ICS setiap enam bulan sekali.<br/></p>
                                <p></p></div>
                                <div><img class="left_img img-thumbnail" src="<?php echo base_url(); ?>assets/images/SMK 3.jpg" alt="SMK 3" />
                                <p> <b>Sistem Manajemen Keselamatan dan Kesehatan Kerja (SMK3:1996)</b><br>
                                Perseroan menyadari bahwa karyawan adalah aset paling berharga. Perseroan bertanggung jawab dalam melindungi karyawan agar kesehatan dan keselamatan tetap terjaga. Sejak tahun 2001, perseroan telah menerapkan Sistem Manajemen Keselamatan dan Kesehatan Kerja (SMK3) guna menciptakan kondisi lingkungan kerja yang aman, sehat dan sejahtera, bebas dari kecelakaan dan pencemaran serta penyakit akibat kerja. Dalam penerapan SMK3, seluruh perseroan nasional masih mengacu kepada Permenaker nomor: 5/Menaker/1996 termasuk PT Semen Tonasa. Dan saat ini, perseroan dinyatakan masih dapat mempertahankan sertifikat dan “Bendera emas” dalam penerapan SMK3 tersebut yang diberikan oleh kementrian Tenaga Kerja Republik Indonesia. Untuk SMK3 ini, audit dilakukan juga oleh PT. Sucofindo ICS setiap tiga tahun sekali.<br/></p>
                                <p></p></div>
                                <div><p> <b>ISPS CODE (International Code for the Security of Ships and Port Facilities)</b><br/>
                                Sejak tahun 2005, rancangan keamanan dan fasilitas pelabuhan khusus Biringkassi telah memenuhi ketentuan SOLAS 74 – Safety Of Life At Sea 1974, Bab X1-2 dan Bagian A dari Peraturan Internasional ISPS Code. Dengan menerapkan ketentuan ISPS Code, Manajemen Perseroan berharap agar aktivitas dan fasilitas Pelabuhan khusus Biringkassi dapat berjalan aman, tertib, dan bersih. Untuk ISPS Code ini, perseroan memperoleh sertifikat/Statement of Compliance of A Port Facility no: 02/0155-DV melalui Direktorat Jenderal Perhubungan Laut RI, yang diverifikasi/diaudit setiap lima tahun sekali dan dilakukan verifikasi/audit antara setiap dua setengah tahun. <br/></p>
                                <p></p></div>
                                <div><img class="left_img img-thumbnail" src="<?php echo base_url(); ?>assets/images/ISO 18001.jpg" alt="ISO 18001" />
                                <p> <b>OHSAS 18001:2007 (Occupational Health & Safety Assessment Series)</b><br>
                                Perseroan memperoleh pengakuan dalam penerapan System Manajemen Keselamatan dan Kesehatan Kerja(SMK3) yang bertaraf internasional pada tahun 2009 dari PT Sucofindo ICS. Sertifikat OHSAS 18001 merupakan wujud dari komitmen Perseroan dalam menciptakan kondisi lingkungan kerja yang aman, sehat, bebas dari kecelakaan dan pencemaran, serta penyakit akibat kerja sehingga keselamatan dan kesehatan karyawan tetap terjamin, dan diakui oleh dunia internasional. OHSAS juga dilakukan audit setiap enam bulan sekali. <br/></p>
                                <p></p></div>
                                <div><p> <b>System Manajemen Laboraturium (ISO/ IEC 17025;2005)</b><br/>
                                Ketepatan dan keabsahan hasil pengujian merupakan cerminan Laboraturium pengujian perseroan. Hal ini juga sebagai wujud nyata dalam memberikan jaminan mutu kepada pelanggan. Pada tanggal 13 Januari 2011, perseroan memperoleh pengakuan dalam melakukan pengujian yaitu Sertifikat ISO/IEC 17025, yang diberikan oleh Komite Akreditasi Nasional (KAN) dengan Sertifikat Nomor: LP-498-IDN. Sehingga dengan demikian, perseroan bukan hanya dapat melakukan pengujian produk sendiri (Internal)tetapi juga dapat melakukan pengujian produk pihak lain (eksternal) tentunya dengan mengacu pada Standar Internasional ISO/IEC 17025.<br/></p>
                                <p></p></div>
                                 <div><p> <b>System Manajemen Terintegrasi (Integrated Management System-IMS)</b><br/>
                                Dalam rangka meningkatkan implementasi keseluruhan system manajemen yang ada dan agar Sistem Manajemen Semen Tonasa (SMST) dapat berjalan dengan efektif dan efisien dalam pencapaian tujuan perseroan, pada tahun 2009 perseroan telah melakukan integrasi system manajemen dan telah mendapat pengakuan dari PT Sucofindo ICS atas penerapan system manajemen yang terintegrasi tersebut.<br/></p>
                                 </div>
                                 <br>

                                <div class="dividerHeading">
                                    <h4><span>Penghargaan</span></h4>
                                </div>

                                    <div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                <i class="switch fa fa-plus"></i>
                                                    How do I download League Template ?
                                                </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                            <i class="switch fa fa-plus"></i>
                                                                How do I download League Template ?
                                                            </a>
                                                            </h4>
                                                        </div>
                                            
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                        </div>      
                                    </div>
                        </div>

                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>News</span></h4>
                            </div>
                            <p>Nunc et magna nisi. lore Aliquam at erat in lorem purus aliquet mollis. Fusce elementum velit vel dolor iaculis. </p>
                            <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <!-- <h4><span>Date</span></h4> -->
                            </div>
                            <ul class="datepicker">
                                <li>
   
                                </li>
                            </ul>
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Sertifikasi</span></h4>
                            
                        </div>                       
                    </div>
                </div>
            </div>
        </section>
    </section>
    <!--end wrapper-->

    <!--start footer-->
<?php $this->load->view('layout/footer') ?>
    <!--end footer-->
    
    
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
    <script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>

    <script src="js/main.js"></script>

    <!-- Start Style Switcher -->
    <div class="switcher"></div>
    <!-- End Style Switcher -->

    
</body>
</html>
