<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Header-->
<?php $this->load->view('layout/header') ?>
                
<!--End Header-->
		
		<section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>Date</span></h4>
                            </div>
                            <ul class="datepicker">
                                <li>
                                    
                                </li>
                                
                            </ul>
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Opini</span></h4>
                            </div>
                            <p><h3><b>Tonasa Jurnalis Award 2014</b></h3></p>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse02">
                                            <i class="switch fa fa-plus"></i>
                                        Kategori Karya Berita Jurnalis
                                        </a>
                                    </h4>
                                </div>
                            <div id="collapse02" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="list_style star">
                                      <li><a href="">Merajut Rupiah Dari Bank Sampah</a></li>
                                      <li><a href="">Semangat Juang Unggul Atas Semen Tonasa Bagi Rakyat Pangkep</a></li>
                                      <li><a href="">Senyum Kehidupan di Kampung Tepian Tonasa</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <i class="switch fa fa-plus"></i>
                                        Kategori Karya Opini Jurnalis
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="list_style star">
                                      <li><a href="">Tonasa Tangkap Logam Berat</a></li>
                                      <li><a href="">Di Biringere, Industri dan Lingkungan Bisa Akur</a></li>
                                      <li><a href="">Ketika Alam dan Masyarakat Bangga Dengan Semen Tonasa</a></li>
                                      <li><a href=""> Tonasa Sahabat dan Roh Pengusaha UKM</a></li>
                                    </ul>
                                </div>
                            </div>

                            <p><h3><b>Tonasa Jurnalis Award 2015</b></h3></p>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            <i class="switch fa fa-plus"></i>
                                                Kategori Karya Berita Jurnalis
                                        </a>
                                    </h4>
                                </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="list_style star">
                                      <li><a href="">Kolaborasi Bandeng Pangkep</a></li>
                                      <li><a href="">Menempa Baja, Menempa Hidup</a></li>
                                      <li><a href="">Senyum Bunaing, Senyum Warga Kampung Baru</a></li>
                                      <li><a href="">Dongkrak Kualitas Produk UKM Lokal</a></li>
                                      <li><a href="">Pundi-Pundi Rupiah dari Limbah Plastik</a></li>
                                      <li><a href="">Dari Bedah Rumah Hingga Pengolahan Pupuk Kompos</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                        <i class="switch fa fa-plus"></i>
                                        Kategori Karya Opini Jurnalis
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="list_style star">
                                      <li><a href="">Sedia Payung Sebelum Hujan Asam</a></li>
                                      <li><a href="">Proper Emas Jelang Tahun Emas</a></li>
                                      <li><a href="">Tonasa ''Serpihan Surga'' di Tanah Pangkep</a></li>
                                      <li><a href="">Bukan Sekedar Semen Bangunan, Tetapi Juga 'Semen Kemandirian Masyarakat'</a></li>
                                      <li><a href="">Semen Tonasa Icon CSR</a></li>
                                      <li><a href="">CSR Cinta PT Semen Tonasa</a></li>
                                    </ul>
                                </div>
                            </div>

                            <p><h3><b>Tonasa Jurnalis Award 2016</b></h3></p>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                            <i class="switch fa fa-plus"></i>
                                            Kategori Karya Berita Jurnalis
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="list_style star">
                                            <li><a href="">Dg Bani, Mimpi Kuliahkan Anak dari Jual Semen Tonasa</a></li>
                                            <li><a href="">Semen Tonasa Terdepan Bangun Peradaban Sulsel</a></li>
                                            <li><a href="">Kolaborasi Di Timur Indonesia</a></li>
                                            <li><a href="">Menanti Underpass, Ikon Baru Infrastruktur Makassar</a></li>
                                            <li><a href="">Menapak Sejarah Menara Pinisi UNM</a></li>
                                            <li><a href="">Underpass Simpang Lima, Ikon Baru Kota Makassar</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                            <i class="switch fa fa-plus"></i>
                                            Kategori Karya Opini Jurnalis
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseSix" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="list_style star">
                                            <li><a href="">Gypsum Membuat Semen Tonasa Selalu di Hati</a></li>
                                            <li><a href="">Kuat Diterpa Panas, Hujan, dan Zaman</a></li>
                                            <li><a href="">Semen Tonasa Tetap MenTonasa di Hati</a></li>
                                            <li><a href="">Konsistensi Semen Tonasa Menghasilkan Produk Unggul Berkualitas</a></li>
                                            <li><a href="">Kiprah Tonasa Dukung Pembangunan Infrastruktur KTI</a></li>
                                            <li><a href="">Tak Sekedar Merekatkan Bahan Bangunan</a></li>
                                        </ul>
                                    </div>
                                </div>
                        </div>

                        </div>

                            
                            

                        </div>
                        </div>
                        
                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>News</span></h4>
                            </div>
                            <p>Nunc et magna nisi. lore Aliquam at erat in lorem purus aliquet mollis. Fusce elementum velit vel dolor iaculis. </p>
                            <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                
            </div>
        </section>
	</section>
	<!--end wrapper-->

	<!--start footer-->
<?php $this->load->view('layout/footer') ?>
	<!--end footer-->
	
	
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
    <script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>

    <script src="js/main.js"></script>

    <!-- Start Style Switcher -->
    <div class="switcher"></div>
    <!-- End Style Switcher -->

	
</body>
</html>
