<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Header-->
<?php $this->load->view('layout/header') ?>
                
<!--End Header-->
		
		<section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>Date</span></h4>
                            </div>
                            <ul class="datepicker">
                                <li>
                                    
                                </li>
                                
                            </ul>
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Profil Komisaris</span></h4>
                            </div>
                            <h3 align="left">Komisaris Utama : Prof. Dr. Idrus Paturusi</h3>
                            <p><img align="left" src="<?php echo base_url(); ?>assets/images/kom01.jpg"></p>
                            <ol style="margin-left:188px">
                                <li> Komisaris Utama PT Semen Tonasa sejak 19 Juni 2012</li>
                                <li> Rektor Universitas Hasanuddin sejak tahun 2006</li>
                                <li> Memperoleh gelar S-3 Ilmu Kedokteran di Universitas Hasanuddin pada tahun 1999</li>
                                <li> Spesialisasi Ahli Bedah Orthopedi dari Universitas Indonesia tahun 1984</li>
                                <li> Post Graduate Orthopaedic Training di Hospital Raymond Point Care, Paris Perancis tahun 1987</li>
                                <li> Post Graduate Orthopedic Training Singapore tahun 1979</li>
                                <li> Mengambil spesialisasi Ahli Bedah Umum dari Laboratorium Ilmu Bedah di Universitas Hasanuddin tahun 1979</li>
                                <li> Sarjana Kedokteran dari Fakultas Kedokteran Universitas Hasanuddin tahun 1977</li>
                            </ol>
                            <h3 align="left">Komisaris Utama : Ir. Andi Herry Iskandar, Msi</h3>
                            <p><img align="left" src="<?php echo base_url(); ?>assets/images/kom02.jpg"></p>
                            <ol style="margin-left:188px">
                                <li> Komisaris PT Semen Tonasa sejak 19 Juni 2012</li>
                                <li> Pelaksana Tugas Bupati Luwu Utara pada tahun 2010</li>
                                <li> Asisten Pemerintahan Sekretaris Daerah Provinsi Sulawesi Selatan sejak tahun 2009</li>
                                <li> Walikota Makassar tahun 2008-2009</li>
                                <li> Wakil Walikota Makassar tahun 2004-2008</li>
                                <li> Kepala Dinas Kelautan dan Perikanan Kota Makassar tahun 2001-2004</li>
                                <li> Program Pasca Sarjana Universitas Hasanuddin-LAN RI tahun 2000</li>
                                <li> Kepala Dinas Pekerjaan Umum; Kab. SInjai, Kab. Gowa & Kota Makassar 1988 - 2001</li>
                                <li> Insinyur Teknik Sipil dari Fakultas Teknik Universitas Hasanuddin pada tahun 1985</li>
                            </ol>
                            <h3 align="left">Komisaris Utama : Ir. Widodo Santoso, MBA</h3>
                            <p><img align="left" src="<?php echo base_url(); ?>assets/images/kom03.jpg"></p>
                            <ol style="margin-left:188px">
                                <li> Komisaris PT Semen Tonasa sejak 19 Juni 2012</li>
                                <li> Direktur Utama PT Semen Padang tahun 2010-2011</li>
                                <li> Direktur Pemasaran PT Semen Padang tahun 2003-2011</li>
                                <li> Direktur Utama PT Semen Kupang tahun 2000-2003</li>
                                <li> Master di bidang Administrasi Niaga dari IMM Jakarta tahun 1993</li>
                                <li> Kadep Pemeliharaan PT Semen Tonasa, Kepala Proyek Tonasa Unit 4, Kadep Pemasaran, 1980 - 1999</li>
                                <li> Insinyur di bidang Teknik Fisika dari Institut Teknologi Sepuluh Nopember tahun 1978</li>
                            </ol>
                            <br><br>
                            <h3 align="left">Komisaris Utama : Ir. Achmad Sigit Dwiwahjono, MPP</h3>
                            <p><img align="left" src="<?php echo base_url(); ?>assets/images/kom04.jpg"></p>
                            <ol style="margin-left:188px">
                                <li> Komisaris PT Semen Tonasa sejak 19 September 2016</li>
                                <li> Direktur Jenderal Industri Kimia, Tekstil, dan Aneka – Kementrian Perindustrian Republik Indonesia, Juli 2016</li>
                                <li> Direktur Jenderal Ketahanan dan Pengembangan Akses Industri Internasional, 2015 - Juli 2016</li>
                                <li> Direktur Pengembangan Fasilitasi Industri Wilayah III 2011-2015</li>
                                <li> Atase Perindustrian di Tokyo 2007 – 2011</li>
                                <li> Kepala Bagian Program dan Evaluasi pada Sekretariat Direktorat Jenderal Industri Agro dan Kimia 2005 – 2007</li>
                                <li> Komite Audit Teknis PT Semen Tonasa 2003</li>
                            </ol>
                        </div>
                        
                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>News</span></h4>
                            </div>
                            <p>Nunc et magna nisi. lore Aliquam at erat in lorem purus aliquet mollis. Fusce elementum velit vel dolor iaculis. </p>
                            <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	</section>
	<!--end wrapper-->

	<!--start footer-->
<?php $this->load->view('layout/footer') ?>
	<!--end footer-->
	
	
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
    <script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>

    <script src="js/main.js"></script>

    <!-- Start Style Switcher -->
    <div class="switcher"></div>
    <!-- End Style Switcher -->

	
</body>
</html>
