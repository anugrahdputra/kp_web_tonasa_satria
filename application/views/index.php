<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="home">
    <header id="header">
        <!-- Start header-top -->
        <div class="header-top">
    <div class="container">
        <div class="row">
            <div class="hidden-xs col-lg-7 col-sm-5 top-info">
                <span><i class="fa fa-phone"></i>Phone: (0410) 312345</span>
                <span class="hidden-sm"><i class="fa fa-envelope"></i>Email: humas.st@semenindonesia.com</span>
            </div>
                <div class="col-lg-5 col-sm-7 top-info clearfix">
                    <ul>
                        <li><a class="my-facebook" href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a class="my-tweet" href=""><i class="fa fa-twitter"></i></a></li>
                        <!-- <li><a class="my-pint" href=""><i class="fa fa-pinterest"></i></a></li> -->
                        <!-- <li><a class="my-rss" href=""><i class="fa fa-rss"></i></a></li> -->
                        <!-- <li><a class="my-skype" href=""><i class="fa fa-skype"></i></a></li> -->
                        <!-- <li><a class="my-google" href=""><i class="fa fa-google-plus"></i></a></li> -->
                        <li>
                            <form class="search-bar">
                                <label for="search" class="search-label">
                                    <button class="search-button"><i class="fa fa-search"></i></button>
                                    <!-- Fix the break-row-bug-->
                                    <input type="text" id="search" class="search-input" />
                                </label>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
</div>

<div id="menu-bar">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-3 col-sm-3">
                        <div id="logo">
                             <h1><a href="index.html"><img src="<?php echo base_url();?>assets/images/logo.png"/></a></h1> 
                        </div>
                    </div> -->
                    
                    <div class="col-lg-12 col-sm-12 navbar navbar-default navbar-static-top container" role="navigation">
                        <!--  <div class="container">-->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="<?php echo base_url()?>"><span class="data-hover"data-hover="home">Beranda</span></a>
                                    <!-- <ul class="dropdown-menu">
                                        <li><a href="index_2.html">Home 2</a></li>
                                        <li><a href="index_3.html">Home 3</a></li>
                                        <li><a href="index_4.html">Home 4</a></li>
                                        <li><a href="index_5.html">Home 5</a></li>
                                    </ul> -->
                                </li>

                                <li><a href="#"><span class="data-hover" data-hover="PROFIL PERUSAHAAN">PROFIL PERUSAHAAN</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url()."profil_singkat/"; ?>">PROFIL SINGKAT</a></li>
                                        <li><a href="<?php echo base_url()."visi_misi/"; ?>">VISI MISI</a></li>
                                        <li><a href="<?php echo base_url()."unit_pabrik/"; ?>">UNIT PABRIK & FASILITAS PENDUKUNG</a></li>
                                        <li>
                                            <a href="#">STRUKTUR MANAJEMEN</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url()."dewan_komisaris/"; ?>">DEWAN KOMISARIS per 2012-2017</a></li>
                                                <li><a href="<?php echo base_url()."dewan_direksi/"; ?>">DIREKSI per 2012-2017</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="<?php echo base_url()."pemegang_saham/"; ?>">PEMEGANG SAHAM</a></li>
                                        <li>
                                            <a href="#">TATA KELOLA PERUSAHAAN</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url()."gcg/"; ?>">GCG</a></li>
                                                <li><a href="<?php echo base_url()."kode_etik/"; ?>">KODE ETIK</a></li>
                                                <li><a href="<?php echo base_url()."budaya_perusahaan/"; ?>">BUDAYA PERUSAHAAN</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="<?php echo base_url()."sdm/"; ?>">SDM</a></li>
                                        <li><a href="<?php echo base_url()."peristiwa_penting/"; ?>">PERISTIWA PENTING</a></li>
                                        <li><a href="<?php echo base_url()."sertifikasi_penghargaan/"; ?>">SERTIFIKASI DAN PENGHARGAAN</a></li>
                                        <li><a href="<?php echo base_url()."laporan_tahunan/"; ?>">LAPORAN TAHUNAN</a></li>
                                    </ul>
                                </li>

                                <li><a href="#" ><span class="data-hover" data-hover="PRODUKSI">PRODUKSI</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url()."jenis_produksi/"; ?>">JENIS PRODUKSI</a></li>
                                        <li><a href="<?php echo base_url()."proses_produksi/"; ?>">PROSES PRODUKSI</a></li>
                                    </ul>
                                </li>
                                <li><a href="#"><span class="data-hover" data-hover="LAYANAN PELANGGAN">LAYANAN PELANGGAN</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url()."pemasaran/"; ?>">PEMASARAN</a></li>
                                        <li><a href="<?php echo base_url()."unit_pengantongan/"; ?>">UNIT PENGANTONGAN SEMEN</a></li>
                                        <li><a href="<?php echo base_url()."jaringan_distribusi/"; ?>">JARINGAN DISTRIBUSI</a></li>
                                        <li><a href="#">SAHABAT TONASA</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url()."faq/"; ?>">FAQ</a></li>
                                                <li><a href="<?php echo base_url()."layanan/"; ?>">LAYANAN PELANGGAN</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li><a href="#"><span class="data-hover" data-hover="CSR">CSR</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url()."csr/"; ?>">CSR</a></li>
                                        <li><a href="#">KEPEDULIAN LINGKUNGAN</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url()."implementasi_afr/"; ?>">IMPLEMENTASI AFR</a></li>
                                                <li><a href="<?php echo base_url()."implementasi_cdm/"; ?>">IMPLEMENTASI CDM</a></li>
                                                <li><a href="<?php echo base_url()."video_csr/"; ?>">VIDEO CSR</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li><a href="#"> <span class="data-hover" data-hover="BERITA">BERITA</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">BERITA</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="views/berita/berita/2013.php">2013</a></li>
                                                <li><a href="views/berita/berita/2014.php">2014</a></li>
                                                <li><a href="views/berita/berita/2015.php">2015</a></li>
                                                <li><a href="views/berita/berita/2016.php">2016</a></li>
                                                <li><a href="views/berita/berita/2017.php">2017</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="views/berita/event.php">EVENT</a></li>
                                        <li><a href="views/berita/video.php">VIDEO</a></li>
                                    </ul>
                                </li>

                                <li><a href="#"> <span class="data-hover" data-hover="PUBLIKASI">PUBLIKASI</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url()."artikel_tja/"; ?>">ARTIKEL TJA</a></li>
                                    </ul>
                                </li>

                                <li><a href="<?php echo base_url()."rekrutmen/"; ?>"> <span class="data-hover" data-hover="REKRUTMEN">REKRUTMEN</span></a>
                                </li>

                                <li><a href="#"> <span class="data-hover" data-hover="AFILIASI">AFILIASI</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url()."ykst/"; ?>">YKST</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                                <!-- <li><a href="#"> <span class="data-hover" data-hover="KONTAK">KONTAK</span></a>
                                </li> -->

                            </ul>
                        </div>
                    <!-- </div> -->
                </div>
            </div>
            <!--/.container -->
        </div>
        <!--/.header-top -->

        
        <!--/#menu-bar -->
        <div class="slider_block">
            <div class="flexslider top_slider">
                <ul class="slides">
                    <li class="slide1">
                        <div class="container">
                            <!-- <div class="flex_caption1">

                                <p class="slide-heading FromTop">Voluptas Assumenda Est </p><br/>

                                <p class="sub-line FromBottom">Dolore Magnam Aliquam Quaerat Voluptatem  </p><br/>

                                <a href="" class="slider-read FromLeft">Download Now</a>
                            </div> -->
                            <!-- <div class="flex_caption2 FromRight"></div>
                        </div> -->
                    </li>

                    <li class="slide2">
                        <div class="container">
                            <div class="slide flex_caption1">
                                <p class="slide-heading FromTop">Edge is our theme</p><br/>

                                <p class="sub-line FromRight">Dolore Magnam Aliquam Quaerat Voluptatem </p><br/>

                                <a href="" class="slider-read FromLeft">Download Now</a>

                            </div>
                            <div class="flex_caption2 FromBottom"></div>
                        </div>
                    </li>
                    <li class="slide3">
                        <div class="container">
                            <div class="slide flex_caption1">
                                <p class="slide-heading FromTop">Easy to Maintain </p><br/>

                                <p class="sub-line FromRight">Powerful Features & Responsive Designs </p><br/>

                                <a href="" class="slider-read FromLeft">Download Now</a>

                            </div>
                            <div class="flex_caption2 FromRight"></div>
                        </div> 
                    </li>
                </ul>
            </div>
        </div>
    </header>
<!--End Header-->

<!--Start Breaking News-->
    <section class="latest_work">
        <div class="container">
            <div class="row sub_content">
                <div class="carousel-intro">
                    <div class="col-md-12">
                        <div class="dividerHeading">
                            <h4><span>Breaking News</span></h4>
                        </div>
                        <div class="carousel-navi">
                            <div id="work-prev" class="arrow-left jcarousel-prev"><i class="fa fa-angle-left"></i></div>
                            <div id="work-next" class="arrow-right jcarousel-next"><i class="fa fa-angle-right"></i></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="jcarousel recent-work-jc">
                    <ul class="jcarousel-list">
                        <!-- Recent Work Item -->

                        <?php foreach($berita as $bb){ ?>
                        <li class="col-sm-3 col-md-3 col-lg-3">
                            <figure class="touching effect-bubba">
                                <img src="<?php echo base_url().'upload/'.$bb->foto_berita; ?>" alt="" class="img-responsive" style="height: 200px;width: 200px;">
                                <div class="option">
                                    <a href="portfolio_single.html" class="fa fa-link"></a>
                                    <a href="<?php echo base_url();?>assets/images/portfolio/portfolio_1.png" class="fa fa-search mfp-image"></a>
                                </div>
                                <figcaption class="item-description">
                                <h5><?php echo $bb->judul_berita; ?></h5>
                                    
                                </figcaption>
                            </figure>
                        </li>
                        <?php } ?>

                    </ul>
                </div>
            </div>
        </div>
    </section>
<!--Start Breaking News-->

<div class="col-lg-12 col-md-12 col-sm-12 wow fadeInDown">
                    <h1 class="intro text-center">Together We Build a Better Future</h1>
                    <!-- <p class="lead text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa nesciunt odit sunt vitae voluptatibus. Ad animi dicta dolore et illo incidunt sint.
                    </p> -->
                </div>



<!-- <section class="wrapper"> -->
<!--start info service-->
   <!--  <section class="info_service">
        <div class="container">
            <div class="row sub_content">
                <div class="rs_box  wow bounceInRight" data-wow-offset="200">
                    <div class="col-sm-6 col-lg-3">
                        <div class="serviceBox_3">
                            <div class="service-icon">
                                <i class="fa fa-laptop"></i>
                            </div>
                            <h3>Web Design</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad culpa dignissimos distinctio ea eius laborum nesciunt qui quis quisquam quos!</p>
                            <a class="read" href="#">Read more</a>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="serviceBox_3">
                            <div class="service-icon">
                                <i class="fa fa-briefcase"></i>
                            </div>
                            <h3>Responsive Desgin</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad culpa dignissimos distinctio ea eius laborum nesciunt qui quis quisquam quos!</p>
                            <a class="read" href="#">Read more</a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="serviceBox_3">
                            <div class="service-icon">
                                <i class="fa fa-mobile"></i>
                            </div>
                            <h3>Retina Ready</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad culpa dignissimos distinctio ea eius laborum nesciunt qui quis quisquam quos!</p>
                            <a class="read" href="#">Read more</a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="serviceBox_3">
                            <div class="service-icon">
                                <i class="fa fa-rocket"></i>
                            </div>
                            <h3>Web Development</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad culpa dignissimos distinctio ea eius laborum nesciunt qui quis quisquam quos!</p>
                            <a class="read" href="#">Read more</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section> -->
<!--end info service-->

<!--start footer-->
<?php $this->load->view('layout/footer') ?>
<!--end footer-->

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url();?>assets/js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.cookie.js"></script> <!-- jQuery cookie -->
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/styleswitch.js"></script> <!-- Style Colors Switcher -->
<!--
<script src="<?php echo base_url();?>assets/js/jquery.fractionslider.js" type="text/javascript" charset="utf-8"></script>
-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.smartmenus.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.smartmenus.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.jcarousel.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jflickrfeed.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/swipe.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flexslider-min.js"></script>

<script src="<?php echo base_url();?>assets/js/main.js"></script>

<!-- Start Style Switcher -->
<div class="switcher"></div>
<!-- End Style Switcher -->
    <script>
        $('.flexslider.top_slider').flexslider({
            animation: "fade",
            controlNav: false,
            directionNav: true,
            prevText: "&larr;",
            nextText: "&rarr;"
        });
    </script>

    <!-- WARNING: Wow.js doesn't work in IE 9 or less -->
    <!--[if gte IE 9 | !IE ]><!-->
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.min.js"></script>
        <script>
            // WOW Animation
            new WOW().init();
        </script>

</body>
</html>