<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Header-->
<?php $this->load->view('layout/header') ?>
                
<!--End Header-->
		
		<section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>Date</span></h4>
                            </div>
                            <ul class="datepicker">
                                <li>
                                    
                                </li>
                                
                            </ul>
                            <!-- <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul> -->
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Profil Perusahaan</span></h4>
                            </div>
                                <img class="left_img img-thumbnail" src="<?php echo base_url(); ?>assets/images/pabrikok.jpg">
                            <p><h2>Umum</h2></p>
                            <p>PT Semen Tonasa adalah produsen semen terbesar di Kawasan Timur Indonesia yang menempati lahan seluas 715 hektar di Desa Biringere, Kecamatan Bungoro, Kabupaten Pangkep, sekitar 68 kilometer dari kota Makassar. Perseroan yang memiliki kapasitas terpasang 5.980.000 ton semen per tahun ini, mempunyai empat unit pabrik, yaitu Pabrik Tonasa II, Pabrik Tonasa III, Pabrik Tonasa IV dan Pabrik Tonasa V. Keempat unit pabrik tersebut menggunakan proses kering dengan kapasitas masing-masing 590.000 ton semen pertahun untuk Unit II dan III, 2.300.000 ton semen per tahun untuk Unit IV serta 2.500.000 ton semen untuk Unit V. Perseroan berdasarkan anggaran dasar merupakan produsen semen di Indonesia yang telah memproduksi serta menjual semen di dalam negeri dan mancanegara sejak tahun 1968. </p>

                            <p>
                            Proses produksi perseroan bermula dari kegiatan penambangan tanah liat dan batu kapur di kawasan tambang tanah liat dan pegunungan batu kapur sekitar pabrik hingga pengantongan semen zak di unit pengantongan semen. Proses produksi perseroan secara terus menerus dipantau oleh satuan Quality Control guna menjamin kualitas produksi. Lokasi pabrik perseroan yang berada di Sulawesi Selatan merupakan daerah strategis untuk mengisi kebutuhan semen di Kawasan Timur Indonesia. Dengan didukung oleh jaringan distribusi yang tersebar dan diperkuat oleh delapan unit pengantongan semen yang melengkapi sarana distribusi penjualan, telah menjadikan perseroan sebagai pemasok terbesar di kawasan tersebut. Kedelapan unit pengantongan semen berlokasi di Bitung, Palu, Banjarmasin dan Ambon dengan kapasitas masing-masing 300.000 ton semen per tahun serta di Makassar, Bali dan Samarinda dengan kapasitas masing-masing 600.000 ton semen per tahun, dan di Pontianak dengan kapasitas 150.000 ton semen per tahun. Sarana pendukung operasi lainnya yang berkontribusi besar terhadap pencapaian laba perusahaan adalah utilitas Pembangkit Listrik Tenaga Uap (PLTU) dengan kapasitas 2x25 MW yang berlokasi di Desa Biringkassi, Kabupaten Pangkep, sekitar 17 km dari lokasi pabrik.    
                            </p>
                            <p>
                            Pendapatan utama perseroan adalah hasil penjualan Semen Portland (OPC), Semen Non OPC yaitu Tipe Komposit (PCC) tersebar di wilayah Sulawesi, Kalimantan, Jawa, Bali, Nusa Tenggara, Maluku dan Papua. Didukung dengan merk produk yang solid di Kawasan Timur Indonesia, perseroan berusaha secara terus menerus mempertahankan brand image produk dengan menjaga kestabilan pasokan produk di pasar semen, selain itu dukungan sistem distribusi yang optimal juga merupakan unsur kesuksesanpenjualan semen perseroan. Disamping itu, penjualan ekspor juga dilakukan perseroan jika terjadi kelebihan produksi setelah pemenuhan pasar dalam negeri. Sejak 15 September 1995 perseroan terkonsolidasi dengan PT Semen Indonesia (Persero) Tbk. (sebelumnya PT Semen Gresik (Persero) Tbk.) menjadi sebuah holding company. Lebih dari satu dekade perseroan berbenah dan berupaya keras meningkatkan nilai perseroan di mata pemegang saham dan stakeholder. Berbagai terobosan strategi dan program kerja dalam meningkatkan kinerja perseroan secara terintegrasi terus dipacu untuk mewujudkan visi perseroan menjadi produsen semen yang terefisien dan mempunyai keunggulan yang kompetitif diantara para produsen semen lainnya. Di mulai tahun 2009 sampai saat ini, perseroan melaksanakan pembangunan Pabrik Tonasa V yang nantinya diharapkan beroperasi dengan kapasitas 2.500.000 ton pertahun dengan dukungan pembangkit listrik 2x35MW dengan pembiayaan proyek tersebut bersumber dari dana sendiri perseroan dan kredit pembiayaan sindikasi perbankan nasional. Pembangkit listrik tersebut di targetkan akan beroperasi normal di tahun 2013.
                            </p>

                            <p></p>

                            <p><h2>Sasaran dan Strategi Perusahaan</h2></p>
                            <p>
                            Sasaran utama perseroan adalah meningkatkan nilai perusahaan kepada shareholders dan stakeholder dengan strategi yang berfokus pada kegiatan bisnis utama, yaitu menambang, memproduksi dan memasarkan produksinya untuk menjamin sustainability perseroan dalam jangka panjang. Perseroan juga berkomitmen untuk mempertahankan kekuatan finansialnya dengan manajemen likuiditas yang sehat untuk memenuhi pembiayaan investasi dan pembayaran kewajiban perusahaan serta pertumbuhan arus kas perseroan secara berkelanjutan. Selain itu perseroan terus melakukan inovasi kerja dalam operasional perusahaan, inovasi kerja dipacu utamanya atas kegiatan kegiatan inti produksi yang dapat menjamin sustainabilitas kinerja perseroan.Sustainabilitas perseroan merupakan pendekatan terpadu terhadap kinerja perusahaan di bidang lingkungan, sosial dan ekonomi, dimana ketiga bidang tersebut saling terkait satu sama lain
                            </p>

                            <p></p>

                            <p><h2>Indikator Kinerja</h2></p>
                            <p>
                            Perseroan menggunakan volume produksi, penjualan, laba komprehensif, ebitda serta rasio keuangan sebagai indikator kunci kinerja. Perseroan dianggap berkinerja bagus jika berhasil melampaui target produksi dan penjualan maupun laba komprehensif serta ebitda. Selain itu perseroan berkewajiban mempertahankan covenant ratio atas DSR dan DSCR yang telah ditetapkan dalam perjanjian kredit pembiayaan Pabrik Tonasa V yang pembiayaannya memperoleh sumber dana pinjaman dari PT. Bank Mandiri (Persero) Tbk. dengan sindikasi banknya. Sedangkan untuk proyek pengembangan baru, IRR minimum merupakan target pengembalian yang diharapkan perusahaan
                            </p>

                            <p></p>

                            <p><h2>Sumber Pendapatan Perseroan</h2></p>
                            <p>
                            Sumber pendapatan perseroan yang besar berasal dari hasil penjualan semen dalam negeri, khususnya di Kawasan Timur Indonesia. Kondisi saat ini, konsumsi semen nasional yang tinggi telah memberikan keuntungan harga yang kompetitif bagi produsen semen nasional. Oleh karena itu, pasar semen dalam negeri tetap merupakan pasar utama yang potensial untuk memperoleh keuntungan yang optimal. Mengingat tantangan yang semakin meningkat ke depan, perseroan tidaklah terlena menikmati kondisi tersebut. Dengan penuh kesadaran, manajemen senantiasa melakukan berbagai strategi alternatif terbaik yang dapat meningkatkan performa perseroan dengan efisiensi operasional yang optimal dan strategi keuangan perseroan yang kuat.
                            </p>

                            <p></p>

                            <p><h2>Konsumen dan Pasar</h2></p>
                            <p>
                            Perseroan berupaya meningkatkan loyalitas pelanggan di daerah pasar pemasaran dengan berbagai langkah. Menjalin kerjasama yang baik dengan para distributor sebagai mediator bisnis serta turut serta dalam pembangunan berbagai proyek infrastruktur merupakan upaya yang dilakukan oleh perseroan untuk terus mengembangkan pangsa pasar.
                            </p>

                            <p></p>

                            <p><h2>Perusahaan Afiliasi Perseroan</h2></p>
                            <p>
                            Lebih dari dua dekade, perseroan dalam menjalankan bisnisnya didukung oleh perusahaan afiliasi yang berlokasi di sekitar perusahaan dengan bidang bisnis yang saling berhubungan dengan bisnis utama perseroan. Dukungan bisnis tersebut dibidang transportasi darat dan laut, tenaga kerja bongkar muat angkutan semen, pengelola pensiun karyawan perusahaan serta bidang konstruksi beton dan jasa bengkel. Berikut disajikan informasi singkat tentang perusahaan afiliasi tersebut. PT Prima Karya Manunggal merupakan perusahaan yang variatif dalam mengembangkan bidang usahanya. Selain sebagai penyedia jasa konstruksi dan pengangkutan darat untuk semen PT Prima Karya Manunggal juga sebagai distributor produk perseroan. Untuk kegiatan pengangkutan darat bahan mentah dan barang jadi, perseroan mendapat dukungan dari PT EMKL Topabiring sejak Juli 1989. Sedangkan kegiatan strategis perusahaan dalam rangka pengangkutan semen curah melalui laut, PT Pelayaran Tonasa Lines telah setia mendistribusikan produk perseroan ke unit pengantongan yang tersebar di berbagai lokasi sejak Februari 1989. Dalam rangka kegiatan bongkar muat serta yang terkait, sejak Juli 1989 PT Biringkassi Raya telah bermitra dengan perseroan. Untuk menjamin kelancaran pasokan kantong, angkutan darat serta penyediaan tenaga alih daya, perseroan melakukan kerjasamadengan Koperasi Karyawan Semen Tonasa. Selain yang bersifat komersial, perseroan juga membentuk entitas yang bergerak dalam kegiatan untuk mendukung kesejahteraan seluruh pihak terkait. Pada tahun 1966 telah dibentuk Yayasan Kesejahteraan Semen Tonasa (YKST) yang bergerak dalam bidang pendidikan, olahraga, rekreasi kolektif dan sebagainya. Sedangkan dalam rangka memberikan kesinambungan kesejahteraan bagi para purna bakti, perseroan membentuk Dana Pensiun Semen Tonasa sebagai pengelola jaminan hari tua.
                            </p>
                        </div>
                        
                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>News</span></h4>
                            </div>
                            <p>Nunc et magna nisi. lore Aliquam at erat in lorem purus aliquet mollis. Fusce elementum velit vel dolor iaculis. </p>
                            <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="row sub_content">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="dividerHeading">
                            <h4><span>Why Choose Us?</span></h4>

                        </div>
                        <ul class="list_style circle">
                            <li><a href="#"> Donec convallis, metus nec tempus aliquet</a></li>
                            <li><a href="#"> Aenean commodo ligula eget dolor</a></li>
                            <li><a href="#"> Cum sociis natoque penatibus mag dis parturient</a></li>
                            <li><a href="#"> Lorem ipsum dolor sit amet cons adipiscing</a></li>
                            <li><a href="#"> Accumsan vulputate faucibus turpis tortor dictum</a></li>
                            <li><a href="#"> Nullam ultrices eros accumsan vulputate faucibus</a></li>
                            <li><a href="#"> Nunc aliquet tincidunt metus sit amet</a></li>
                        </ul>
                    </div>
                    
                    <!-- TESTIMONIALS -->
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="dividerHeading">
                            <h4><span>What Client's Say</span></h4>

                        </div>
                        <div id="testimonial-carousel" class="testimonial carousel slide">
                            <div class="carousel-inner">
                                <div class="active item">
                                    <div class="testimonial-item">
                                        <div class="icon"><i class="fa fa-quote-right"></i></div>
                                        <blockquote>
                                            <p>Donec convallis, metus nec tempus aliquet, nunc metus adipiscing leo, a lobortis nisi dui ut odio. Nullam ultrices, eros accumsan vulputate faucibus, turpis tortor dictum.</p>
                                        </blockquote>
                                        <div class="icon-tr"></div>
                                        <div class="testimonial-review">
                                            <img src="images/testimonials/1.png" alt="testimoni">
                                            <h1>Jonathan Dower,<small>Company Inc.</small></h1>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="item">
                                    <div class="testimonial-item">
                                        <div class="icon"><i class="fa fa-quote-right"></i></div>
                                        <blockquote>
                                            <p>Nunc aliquet tincidunt metus, sit amet mattis lectus sodales ac. Suspendisse rhoncus dictum eros, ut egestas eros luctus eget. Nam nibh sem, mattis et feugiat ut, porttitor nec risus.</p>
                                        </blockquote>
                                        <div class="icon-tr"></div>
                                        <div class="testimonial-review">
                                            <img src="images/testimonials/2.png" alt="testimoni">
                                            <h1>Jonathan Dower<small>Leopard</small></h1>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="testimonial-buttons"><a href="#testimonial-carousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>&#32;
                            <a href="#testimonial-carousel" data-slide="next"><i class="fa fa-chevron-right"></i></a></div>
                        </div>
                    </div><!-- TESTIMONIALS END -->
                </div>
            
                <!-- <div class="row sub_content">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="promo_box">
                            <div class="col-sm-9">
                                <div class="promo_content">
                                    <h3>Edge is awesome responsive template, with refreshingly clean design.</h3>
                                    <p>Lorem ipsum dolor sit amet, cons adipiscing elit. Aenean commodo ligula eget dolor. </p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="pb_action">
                                    <a href="#fakelink" class="btn btn-default btn-lg">
                                        <i class="fa fa-shopping-cart"></i>
                                        Download Now
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="row  sub_content">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="dividerHeading">
                            <h4><span>Meet the Team</span></h4>

                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="our-team">
                            <div class="pic">
                                <img src="images/teams/1.png" alt="profile img">
                                <div class="social_media_team">
                                    <ul class="team_social">
                                        <li><a class="fb" href="#." data-placement="top" data-toggle="tooltip" title="Facbook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="twtr" href="#." data-placement="top" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="gmail" href="#." data-placement="top" data-toggle="tooltip" title="Google"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="team_prof">
                                <h3 class="names">kristiana<small>Web Developer</small></h3>
                                <p class="description">Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commo, magnase quis lacinia ornare, quam ante aliqua nisi, eu iaculis leo purus venenatis scelerisque. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="our-team">
                            <div class="pic">
                                <img src="images/teams/2.png" alt="profile img">
                                <div class="social_media_team">
                                    <ul class="team_social">
                                        <li><a class="fb" href="#." data-placement="top" data-toggle="tooltip" title="Facbook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="twtr" href="#." data-placement="top" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="gmail" href="#." data-placement="top" data-toggle="tooltip" title="Google"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="team_prof">
                                <h3 class="names">williamson<small>Web Developer</small></h3>
                                <p class="description">Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commo, magnase quis lacinia ornare, quam ante aliqua nisi, eu iaculis leo purus venenatis scelerisque. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="our-team">
                            <div class="pic">
                                <img src="images/teams/3.png" alt="profile img">
                                <div class="social_media_team">
                                    <ul class="team_social">
                                        <li><a class="fb" href="#." data-placement="top" data-toggle="tooltip" title="Facbook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="twtr" href="#." data-placement="top" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="gmail" href="#." data-placement="top" data-toggle="tooltip" title="Google"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="team_prof">
                                <h3 class="names">miranda joy<small>Web Desginer</small></h3>
                                <p class="description">Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commo, magnase quis lacinia ornare, quam ante aliqua nisi, eu iaculis leo purus venenatis scelerisque. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="our-team">
                            <div class="pic">
                                <img src="images/teams/4.png" alt="profile img">
                                <div class="social_media_team">
                                    <ul class="team_social">
                                        <li><a class="fb" href="#." data-placement="top" data-toggle="tooltip" title="Facbook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="twtr" href="#." data-placement="top" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="gmail" href="#." data-placement="top" data-toggle="tooltip" title="Google"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="team_prof">
                                <h3 class="names">steve thomas<small>Web Developer</small></h3>
                                <p class="description">Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commo, magnase quis lacinia ornare, quam ante aliqua nisi, eu iaculis leo purus venenatis scelerisque. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	</section>
	<!--end wrapper-->

	<!--start footer-->
<?php $this->load->view('layout/footer') ?>
	<!--end footer-->
	
	
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
    <script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>

    <script src="js/main.js"></script>

    <!-- Start Style Switcher -->
    <div class="switcher"></div>
    <!-- End Style Switcher -->

	
</body>
</html>
