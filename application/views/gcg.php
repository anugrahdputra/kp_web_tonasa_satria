<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Header-->
<?php $this->load->view('layout/header') ?>
                
<!--End Header-->
        
        <section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>Date</span></h4>
                            </div>
                            <ul class="datepicker">
                                <li>
                                    
                                </li>
                                
                            </ul>
                            <!-- <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul> -->
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>GCG</span></h4>
                            </div>
                                <!-- <img class="left_img img-thumbnail" src="<?php echo base_url(); ?>assets/images/pabrikok.jpg"> -->
                                <img class="left_img img-thumbnail" src="<?php echo base_url(); ?>assets/images/gcg.jpg" alt="GCG" />
                                <p>
                                    Dalam rangka meningkatkan keunggulan daya saing, perseroan memiliki komitmen yang tinggi untuk menerapkan praktik terbaik Good Corporate Governance (GCG) untuk memaksimalkan nilai perseroan dengan menjamin peningkatan kinerja perseroan dan pertumbuhan bisnis yang berkelanjutan kepada pemegang saham dan para pemangku kepentingan lainnya. 
                                </p>
                                <p>
                                    Penerapan governance atau tata kelola perseroan berdasarkan Peraturan Menteri Badan Usaha Milik Negara Nomor 01/MBU/2011, diartikan sebagai suatu proses dan struktur yang digunakan oleh organ perseroan yang meliputi Rapat Umum Pemegang Saham, Dewan Komisaris dan Dewan Direksi dalam meningkatkan keberhasilan usaha dan akuntabilitas perseroan guna mewujudkan nilai pemegang saham dalam jangka panjang dengan tetap memperhatikan kepentingan stakeholder, berlandaskan peraturan perundang-undangan dan etika berusaha. 
                                </p>
                                <p>
                                    Tata kelola perusahaan merupakan seperangkat peraturan yang mengatur hubungan antara Dewan Komisaris dengan Dewan Direksi serta pelaksanaan RUPS, yang dilakukan sebagai alat pertanggungjawaban masing-masing organ tersebut terhadap pemegang saham dan pemangku kepentingan perseroan. Pelaksanaan tata kelola yang baik juga membutuhkan pengelolaan manajemen perseroan yang mengatur hubungan antara direksi dengan organ-organ yang ada di bawahnya termasuk karyawan sebagaimana diatur dalam anggaran dasar perseroan. 
                                </p>
                                <P>
                                    Sejalan dengan program tranformasi korporasi dan inovasi yang terus berkembang, perseroan senantiasa berupaya melengkapi berbagai pranata organisasi dan membangun mekanisme pengelolaan bisnis yang andal. Hal ini diwujudkan melalui penerapan praktik-praktik tata kelola perseroan yang baik pada seluruh tingkat dan jenjang organisasi secara konsisten. Implementasi governance ditujukan untuk memastikan terciptany menyeluruh bagi seluruh pemegang saham dan seluruh pemangku kepentingan, baik ekonomia keseimbangan bisnis secara maupun sosial, individu dengan kelompok, internal juga eksternal, jangka pendek dan jangka panjang. 
                                </P>

                        </div>
                        
                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>News</span></h4>
                            </div>
                            <p>Nunc et magna nisi. lore Aliquam at erat in lorem purus aliquet mollis. Fusce elementum velit vel dolor iaculis. </p>
                            <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <!--end wrapper-->

    <!--start footer-->
<?php $this->load->view('layout/footer') ?>
    <!--end footer-->
    
    
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
    <script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>

    <script src="js/main.js"></script>

    <!-- Start Style Switcher -->
    <div class="switcher"></div>
    <!-- End Style Switcher -->

    
</body>
</html>
