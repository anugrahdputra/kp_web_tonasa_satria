<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Header-->
<?php $this->load->view('layout/header') ?>
                
<!--End Header-->
		
		<section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>Date</span></h4>
                            </div>
                            <ul class="datepicker">
                                <li>
                                    
                                </li>
                                
                            </ul>
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Frequently Asked Questions</span></h4>
                            </div>
                            <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse02">
                                        <i class="switch fa fa-plus"></i>
                                        1. Jenis produk semen yang beredar di Indonesia apa saja? 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse02" class="panel-collapse collapse">
                                <div class="panel-body">Sesuai dengan kebutuhan, berbagai macam produk semen sudah beredar di Indonesia. Jenis produk semen tersebut untuk karakteristik penggunaan ada semen untuk bangunan, jembatan, jalan, pelabuhan,dan untuk karakteristik lingkungan korosif dan lainnya. </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <i class="switch fa fa-plus"></i>
                                        2. Adakah standar sebuah produk semen?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">Produk semen diatur menurut standar internasional yaitu ASTM dan standar nasional bagi semen yang beredar di Indonesia yaitu Standar Nasional Indonesia (SNI). Dalam rangka perlindungan konsumen Pemerintah melalui Badan Standarisasi Nasional melakukan pengujian terhadap produk semen yang beredar di Indonesia apakah sudah sesuai SNI:</div>
                                <div class="panel-body"><ul class="list_style">
                                    <li>Portland Cement Tipe I (PC I) = SNI 15-2049-04</li>
                                    <li>Portland Pozzolan Cement (PPC) = SNI 15-2049-04</li>
                                    <li>Super Masonry Cement (SMC) = SNI 15-3500-1993</li>
                                    <li>Oil Well Cement (OWC) = SNI 15-3044-1992</li>
                                    <li>Portland Composite Cement (PCC) = SNI 15-700-2004</li>
                                    <li>Portland Pozzolan Cement (PPC) = SNI 15-0302-94</li> 
                                </ul></div>
                                <div class="panel-body">SNI merupakan jaminan bahwa produk tersebut berkualitas dan telah disesuaikan dengan karakteristik bangunan di Indonesia. Tentunya produk semen yang standarnya lebih dari SNI akan lebih memberikan jaminan produk, mutu dan usia bangunan yang akan lebih lama. Untuk mendapatkan informasi mengenai SNI produk semen dapat dilihat di www.bsn.or.id.</div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <i class="switch fa fa-plus"></i>
                                        3. Bagaimanakah ciri fisik sebuah produk semen berkualitas? 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">Pengenalan produk semen yang berkualitas antara lain dapat dikenali dari ciri fisik yaitu warna semen. Warna semen sangat dipengaruhi oleh kadar MgO, apabila semen semakin tinggi kandungan MgO maka menjadi lebih gelap yang dapat mengakibatkan semen akan mudah retak apabila dipergunakan, oleh karena itu MgO dibatasi hanya maksimum 2 %, Typical kadar MgO SG sekitar 0.5  1.5 %. Warna ideal untuk semen adalah Abu-abu kehijauan (Grey greenish ) warna tersebut dibentuk oleh reaksi MgO,FeO, dan Aluminat di dalam kiln. (warna tersebut bisa dikondisikan dengan pengaturan kadar oksida oksida tersebut. </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                        <i class="switch fa fa-plus"></i>
                                        4. Ada berapa macam produk PT Semen Tonasa dan kemasannya? 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse">
                                <div class="panel-body">Semen Tonasa memproduksi dua macam semen yaitu SEMEN PROTLAND TYPE I atau biasa disebut ORDINARY PORTLAND CEMENT (OPC) yang dikemas dalam 50 kg dan PORTLAND POZZOLAN CEMENT (PPC) yang dikemas dalam kantong 40 & 50 kg. Kedua type semen tersebut juga dijual dalam bentuk curah dan dan jumbo bag 1 ton & 1,5 ton.</div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                        <i class="switch fa fa-plus"></i>
                                        5. Apa ada perbedaan semen OPC dan PPC? 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse">
                                <div class="panel-body">OPC digunakan untuk bangunan umum yang tidak memerlukan persyaratan khusus seperti untuk membangun gedung bertingkat, jalan raya, landasan pacu pesawat dll sedangkan PPC juga digunakan untuk bangunan umum dan mempunyai kegunaan khusus yaitu untuk bangunan yang memerlukan ketahanan terhadap garam laut, sulfat dengan panas hidrasi sedang. Bangunan - bangunan yang telah menggunakan Semen Tonasa OPC antara lain Tugu Mandala, masjid Almarkas Al Islami, gedung Clarion, Fly Over dan gedung-gedung bertingkat yang lain. Sedangkan bangunan - bangunan yang telah menggunakan ST PPC antara lain, , pelabuhan Sukarno Hatta di Makassar, Anjungan di Pantai Losari dll.</div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                                        <i class="switch fa fa-plus"></i>
                                        6. Apa perbedaan mutu antara Semen Tonasa kantong dengan tulisan Hitam-Hitam dengan kantong tulisan Hitam-Merah? Apa perbedaan Komposisi Kantong yang di jual di daerah Sulsel dengan di daerah luar Sulsel. 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse">
                                <div class="panel-body">Seluruh produk ST yang dipasarkan dijamin mutunya baik yang dikemas dalam kantong tulisan Hitam - Hitam, kantong tulisan Hitam - Merah, jumbo maupun dalam bentuk curah. Perbedaan tulisan dalam kantong ST digunakan untuk membedakan daerah pemasaran saja. Kantong ST di pasar P Sulawesi dalam bentuk Kraft Paper 3 ply sedangkan kantong ST untuk luar Pulau Sulawesi dalam bentuk woven laminasi kraft (sering disebut bungkus plastic/Kertas Kop) karena handling ke luar pulau membutuhkan penanganan khusus yaitu dengan menggunakan angkutan kapal. </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
                                        <i class="switch fa fa-plus"></i>
                                        7. Bagaimanakah produk PT Semen Tonasa dibandingkan dengan SNI? 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseEight" class="panel-collapse collapse">
                                <div class="panel-body">Semua produk PT Semen Tonasa telah lulus uji SNI, bahkan semua jenis produk PT Semen Tonasa memiliki kualitas diatas persyaratan SNI, antara lain : </div>
                                <div class="panel-body"><ul>
                                            <li>MgO (Magnesium Oksida), persyaratan SNI < 0,6% sedangkan Semen Tonasa 0,89%. Jika kandungan MgO lebih dari 5% akjan menimbulkan retak dan keruntuhan bangunan.</li>
                                            <li>Pemuaian, persyaratan SNI < 0,8% sedangkan Semen Tonasa hanya 0,04% (kecil sekali pemuaiannya). Pemuaian yang besar akan menyebabkan bangunan retak.</li>
                                            <li>Kuat tekan, baik dari pengujian 3, 7 dan 28 hari berdasarkan standar SNI, maka hasil uji produk Semen Gresik rata-rata mencapai 160% dari persyaratan (jauh diatas persyaratan).</li>    
                                </ul></div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
                                        <i class="switch fa fa-plus"></i>
                                        8. Bagaimanakah cara penggunaan produk PT Semen Tonasa supaya hasilnya maksimal?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseNine" class="panel-collapse collapse">
                                <div class="panel-body">Setiap kondisi lingkungan dan bangunan membutuhkan karakteristik hasil penggunaan semen yang berbeda. Untuk informasi lebih lanjut silajkan kunjungi website kami di <a href="www.semen tonasa.co.id"></a></div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
                                        <i class="switch fa fa-plus"></i>
                                        9. Bagaimanakah manajemen mengkontrol kualitas produk PT Semen Tonasa supaya selalu memenuhi standar SNI dan memberikan kepuasan pada pelanggan? 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTen" class="panel-collapse collapse">
                                <div class="panel-body">Untuk menjamin mutu dari produk PT Semen Tonasa, manajemen menerapkan kebijakan dan manajemen sebagai berikut : </div>
                                <div class="panel-body"><ul class="list_style">
                                    <li>Pemilihan jenis bahan baku yang baik yaitu bahan pozzolan trass dan fly ash. Pemilihan jenis batu kapur (lime stone) tertentu dan membatasi prosentase dolomite (batu kapur dengan MgO tinggi). Manajemen produksi yang selalu meng-update technology proses sesuai perkembangan teknologi yang ada, proses quality control automatic sampling, quality control by X ray (QCX) on line dengan peralatan produksi, raw material stockplie control dengan Geoscan system, pengendalian operasi peralatan yang tersentral dari central control room dengan CDS system. </li>
                                    <li>Sertifikasi laboratorium mutu yang diakjreditasi oleh Komite Akreditasi Nasional (KAN). </li>
                                    <li>Sertifikasi standar mutu internasional, antara lain : ISO 9001:2000, ISO 14001:2004, Sistim Manajemen Keselamatan Kerja, Malcolm Baldrige, Penarapan Good Corporate Governance dan masih banyak lagi. Untuk lebih detail dapat di lihat di www.sementonasa.co.id</li>
                                    <li>Pengukuran kepuasan pelanggan melalui : customer satisfaction measurement, channel satisfaction measurement and tracking, quick count per area di seluruh pulau Jawa</li>
                                    <li>Sistim saran dan 5R dilingkungan PT Semen Gresik.</li>
                                </ul></div>
                            </div>
                        </div>


                        </div>
                        </div>
                        
                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>News</span></h4>
                            </div>
                            <p>Nunc et magna nisi. lore Aliquam at erat in lorem purus aliquet mollis. Fusce elementum velit vel dolor iaculis. </p>
                            <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                
            </div>
        </section>
	</section>
	<!--end wrapper-->

	<!--start footer-->
<?php $this->load->view('layout/footer') ?>
	<!--end footer-->
	
	
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
    <script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>

    <script src="js/main.js"></script>

    <!-- Start Style Switcher -->
    <div class="switcher"></div>
    <!-- End Style Switcher -->

	
</body>
</html>
