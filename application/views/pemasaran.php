<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Header-->
<?php $this->load->view('layout/header') ?>
                
<!--End Header-->
		
		<section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>Date</span></h4>
                            </div>
                            <ul class="datepicker">
                                <li>
                                    
                                </li>
                                
                            </ul>
                            <!-- <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul> -->
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Area Pemasaran PT. Semen Tonasa</span></h4>
                            </div>
                                <span class="wpmd"><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="650" height="400" id="baru" align="middle">
                                <param name="allowScriptAccess" value="sameDomain" />
                                <param name="movie" value="<?php echo base_url(); ?>assets/images/areapenjualan1.swf" /><param name="quality" value="high" /><param name="bgcolor" value="#dbf5fe" /><embed src="<?php echo base_url(); ?>assets/images/areapenjualan1.swf" quality="high" width="100%" height="400" name="baru" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
                                </object>
                                <br></span>
                            
                            <p>Keuntungan terbesar perseroan berasal dari hasil penjualan semen dalam negeri dikawasan timur Indonesia. Kondisi saat ini, konsumsi semen dalam negeri tinggi memberikan cukup keuntungan bagi produsen semen nasional. Oleh karena itu, pasar semen dalam negeri tetap merupakan pasar utama yang potensial. Walaupun kondisi pasar dalam negeri sangat potensial, dengan penuh kesadaran perseroan senantiasa melakukan alternatif strategi-strategi yang terbaik untuk meningkatkan efisiensi operasional dan keuangan perseroan. </p>
                            <p>Perseroan berupaya keras menjalin kerjasama yang baik dengan distributor-distributornya sebagai mediator bisnis penjualan retail semen ke konsumen akhir di daerah pasar kawasan timur Indonesia seperti Sulawesi, Kalimantan, Nusa Tenggara, Bali, Ambon dan Papua. Selain itu, perseroan juga membangun kerjasama dengan proyek-proyek pembangunan infrastruktur pemerintah di kawasan tersebut dengan menunjuk distributor yang memadai untuk memediasi suplai semen ke kontraktor proyek</p>
                            <p>Perseroan juga mengekspor kelebihan produksinya ke pasar luar negeri (ekspor). Berbagai negara yang telah berhasil diterobos seperti Kamboja, Filipina, Vietnam, dan beberapa negara tetap seperti Afrika, Bangladesh, Madagaskar, Malaysia, Singapura dan Timor-Timur.</p>
                        </div>
                        
                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>News</span></h4>
                            </div>
                            <p>Nunc et magna nisi. lore Aliquam at erat in lorem purus aliquet mollis. Fusce elementum velit vel dolor iaculis. </p>
                            <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                

                
            </div>
        </section>
	</section>
	<!--end wrapper-->

	<!--start footer-->
<?php $this->load->view('layout/footer') ?>
	<!--end footer-->
	
	
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
    <script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>

    <script src="js/main.js"></script>

    <!-- Start Style Switcher -->
    <div class="switcher"></div>
    <!-- End Style Switcher -->

	
</body>
</html>
