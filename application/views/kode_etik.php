<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Header-->
<?php $this->load->view('layout/header') ?>
                
<!--End Header-->
        
        <section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>Date</span></h4>
                            </div>
                            <ul class="datepicker">
                                <li>
                                    
                                </li>
                                
                            </ul>
                            <!-- <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul> -->
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Kode Etik</span></h4>
                            </div>
                                <p>Dalam upaya lebih mendorong peningkatan produktivitas dan efisiensi perseroan, maka implementasi prinsip-prinsip tata kelola yang baik menjadi pilihan sekaligus kebutuhan yang tidak dapat dihindari lagi. Tata kelola ini diharapkan dapat menjadi value driver dalam menghambat praktik-praktik korupsi, kolusi dan nepotisme, meningkatkan disiplin anggaran, mendayagunakan pengawasan dan mendorong efisiensi pengelolaan perseroan. Tepatnya pada tanggal 18 Januari 2012, perseroan kembali melakukan penerbitan Buku Panduan GCG dan Buku Pedoman Kode Etik setelah beberapa penyesuaian dilakukan. Panduan tata kelola merupakan kristalisasi dari kaidah-kaidah good corporate governance, peraturan perundangundangan yang berlaku, nilai-nilai budaya yang dianut, visi dan misi serta praktik-praktik terbaik governance (best practice). Governance Code ini menjadi acuan bagi pemegang saham, Dewan Komisaris, Dewan Direksi, karyawan serta pihak yang berkepentingan dalam berhubungan dengan perseroan.</p>

                                <p></p>

                                <p>Dalam menjalankan aktivitas bisnisnya, perseroan mengupayakan penerapan standar etika terbaik sejalan dengan visi, misi dan budaya yang dimiliki yang termuat dalam Code of Conduct. Code of Conduct merupakan tuntunan sikap dan perilaku yang dituntut dan berlaku bagi segenap jajaran perseroan. Perseroan menyadari sepenuhnya bahwa hubungan yang baik dengan stakeholder dan peningkatan nilai pemegang saham dalam jangka panjang hanya dapat dicapai melalui integritas bisnis dalam setiap aktivitas bisnis perseroan sebagaimana tercantum Code of Conduct perseroan. Code of conduct mengatur penerapan standar etika dalam seluruh kegiatan bisnis perseroan berdasarkan prinsip-prinsip governance, bahwa : </p>

                                <p></p>

                                <ol>
                                    <li>Segenap individu dan atau organisasi perseroan di lingkungan perseroan senantiasa wajib patuh terhadap hukum dan peraturan yang berlaku dimanapun beroperasi.</li>
                                    <li>Penerimaan manfaat materi atas kekuasaan, jabatan, fungsi dan tugas baik secara langsung ataupun tidak langsung atas janji, pembayaran, tawaran atau penerimaan suap adalah praktik yang dilarang. </li>
                                    <li>Benturan kepentingan antara perseroan dan karyawan, demikian pula benturan kepentingan atas kelompok atau organisasi kerja sedapat mungkin harus dihindari. </li>
                                    <li>Mendukung prinsip-prinsip persaingan usaha sejalan dengan undang-undang persaingan usaha serta peraturan pemerintah terkait. </li>
                                    <li>Menghindari tindakan ilegal, persaingan yang berlebihan dan tidak efektif serta penggunaan praktik yang tidak fair atau perilaku menyimpang dalam upaya meraih laba. </li>
                                    <li>Para pimpinan departemen/biro/seksi/unit kerja perseroan diwajibkan mensosialisasikan panduan tata kelola perusahaan ini untuk mempertahankan dan menegakkan kejujuran, integritas dan keadilan dalam setiap aktifitas di lingkungan masing-masing. </li>
                                    <li>Mengupayakan perolehan informasi melalui cara-cara yang sah dan menyimpan serta menggunakannya sesuai dengan prinsip-prinsip etika usaha yang berlaku. </li>
                                </ol>

                                <p></p>

                                <p> <b>Daftar Dokumen :</b></p>
                                <ul class="list_style right-arrow">
                                    <li><a href="<?php echo base_url(); ?>assets/documents/Charter Komite Audit.pdf" target="_blank"><b> Charter Komite Audit</b></a></li>
                                    <li><a href="<?php echo base_url(); ?>assets/documents/Pedoman Etika Perusahaan.pdf" target="_blank"><b> Pedoman Etika Perusahaan</b></a></li>
                                    <li><a href="<?php echo base_url(); ?>assets/documents/Board Manual.pdf" target="_blank"><b> Board Manual</b></a></li>
                                    <li><a href="<?php echo base_url(); ?>assets/documents/Piagam Internal Audit.pdf" target="_blank"><b>Piagam Internal Audit</b></a></li>
                                    <li><a href="<?php echo base_url(); ?>assets/documents/Pedoman Gratifikasi.pdf" target="_blank"><b>Pedoman Gratifikasi</b></a></li>
                                    <li><a href="<?php echo base_url(); ?>assets/documents/SK Direksi - Kebijakan LHKPN.rar" target="_blank"><b> Dokumen SK Direksi - Kebijakan LHKPN</b></a></li>
                                    <li><a href="<?php echo base_url(); ?>assets/documents/Pedoman WBS.pdf" target="_blank"><b>Pedoman WBS</b></a></li>
                                </ul>
                        </div>
                        
                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>News</span></h4>
                            </div>
                            <p>Nunc et magna nisi. lore Aliquam at erat in lorem purus aliquet mollis. Fusce elementum velit vel dolor iaculis. </p>
                            <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <!--end wrapper-->

    <!--start footer-->
<?php $this->load->view('layout/footer') ?>
    <!--end footer-->
    
    
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
    <script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>

    <script src="js/main.js"></script>

    <!-- Start Style Switcher -->
    <div class="switcher"></div>
    <!-- End Style Switcher -->

    
</body>
</html>
