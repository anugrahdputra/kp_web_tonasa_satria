<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Header-->
<?php $this->load->view('layout/header') ?>
                
<!--End Header-->
		
		<section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>Date</span></h4>
                            </div>
                            <ul class="datepicker">
                                <li>
                                    <?php $this->load->view('calendar') ?>
                                </li>
                                
                            </ul>
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Corporate Social Responsbility</span></h4>
                            </div>
                            <h3><p style="text-align: center;align:center"><b>Tanggung Jawab Sosial Perusahaan</b></p></h3>
                            <p>Sebagai salah satu pemangku kepentingan atas keberadaan perseroan, masyarakat sekitar merupakan bagian dari perseroan yang menjadi indikator sekaligus pihak yang mendapat multiflier effect dari perkembangan bisnis perseroan. Tanggung jawab perseroan kepada masyarakat dan lingkungan sekitar diwujudkan dengan kewajiban perseroan untuk mengalokasikan bagian keuntungan perusahaan guna mendukung peningkatan kualitas kehidupan masyarakat sekitar dibidang ekonomi dan sosial.</p>
                            <p>Perseroan telah menetapkan blueprint CSR sebagai manual book pelaksanaan program-program tanggung jawab sosial dan lingkungan/Corporate Sosial Responsibility (TJSL), yang didalamnya terdapat Strategic Flagship CSR Semen Tonasa yang mengambil tema Konservasi Energi Untuk Negeri.</p>
                            <p>Program-program TJSL yang dilaksanakan oleh perseroan mengacu pada UU No. 40/2007- UU Perseroan Pasal 74 dan penerapan konsep triple bottom lines yang menyelaraskan ekonomi, sosial dan lingkungan. Perseroan berkomitmen sebagaimana tercantum dalam kebijakan perusahaan untuk berperan serta dalam pembangunan ekonomi berkelanjutan guna meningkatkan kualitas kehidupan dan lingkungan yang bermanfaat, baik bagi perseroan sendiri, komunitas setempat, maupun masyarakat pada umumnya. Pembiayaan dana tanggung jawab sosial dan lingkungan dianggarkan sesuai dengan Rencana Kerja dan Anggaran Perusahaan (RKAP) Tahunan.</p>
                            <p>Selain program TJSL, perseroan juga melakukan Penyaluran Program Kemitraan dan Program Bina Lingkungan (PKBL) yang dananya disisihkan maksimal 2% dari laba setelah pajak. Sumber dan penggunaan dana PKBL diatur dalam Peraturan Menteri No.05/ MBU/2007 Pasal 9.</p>
                            <p>Dalam pelaksanaan TJSL sesuai blueprint yang telah ditetapkan, perseroan bermitra dengan Pemkab Pangkep, dinas terkait, lembaga penelitian, LSM, perusahaan lain di Pangkep dan masyarakat lingkar. Perseroan telah menetapkan empat pilar utama yang didalamnya terdapat berbagai program dan kegiatan TJSL. Pilar TJSL tersebut adalah Program Sehat Tonasa, Cerdas Tonasa, Bina Mitra Tonasa dan Desa Mandiri Tonasa yang memiliki sasaran strategis :</p>
                            <ul class="list_style">
                                <li>Meningkatkan kesehatan dan mempromosikan budaya hidup sehat bagi masyarakat lingkar dan karyawan PT Semen Tonasa.</li>
                                <li>Meningkatkan kualitas pendidikan yang berkesinambungan dan memberikan manfaat bersama.</li>
                                <li>Kemitraan dalam menjalankan program ekonomi yang berorientasi pada kemandirian masyarakat.</li>
                                <li>Pengelolaan kawasan desa lingkar untuk mengurangi dampak operasi, kelestarian lingkungan dan dukungan energi.</li>
                            </ul>
                            <p>Sebagai Perseroan yang beroperasi dan berkembang di tengah masyarakat, Semen Tonasa turut bertanggung jawab dalam mendorong kemajuan masyarakat sekitar, dengan berdasar pada tujuh prinsip utama : </p>
                            <ol>
                                <li>Transparansi Dan Akuntabilitas</li>
                                <li>Kearifan Lokal</li>
                                <li>Kejujuran Dan Kepercayaan</li>
                                <li>Keswadayaan</li>
                                <li>Keadilan</li>
                                <li>Kemitraan Dan Kesetaraan</li>
                                <li>Kemandirian</li>
                            </ol>
                            <p><h4><b>Tonasa Bersaudara</b></h4></p>
                            <p>Dalam pelaksanaannya, Program Tanggung Jawab Sosial dan Lingkungan Semen Tonasa dinamakan Tonasa Bersaudara yang memiliki lima pilar, yaitu :</p>
                            <ol>
                                <li><b>>Tonasa Mandiri</b></li>
                                <p>Bentuk partisipasi aktif perusahaan dalam meningkatkan kemandirian ekonomi masyarakat</p>
                                <li><b>Tonasa Cerdas</b></li>
                                <p>Peran serta perusahaan secara aktif dalam meningkatkan kualitas sumber daya manusia</p>
                                <li><b>Tonasa Sehat</b></li>
                                <p>Kepedulian perusahaan dalam meningkatkan kualitas hidup dan kesehatan masyarakat dan lingkungan</p>
                                <li><b>Tonasa Bersahaja</b></li>
                                <p>Kepedulian perusahaan terhadap kondisi sosial dan peran serta aktif terhadap pengembangan aspek seni dan budaya serta olah raga</p>
                                <li><b>Tonasa Hijau</b></li>
                                <p>Pengejawantahan dari komitmen perusahaan dalam pelestarian alam secara berkelanjutan</p>
                            </ol>
                        </div>
                        
                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>News</span></h4>
                            </div>
                            <p>Nunc et magna nisi. lore Aliquam at erat in lorem purus aliquet mollis. Fusce elementum velit vel dolor iaculis. </p>
                            <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	</section>
	<!--end wrapper-->

	<!--start footer-->
<?php $this->load->view('layout/footer') ?>
	<!--end footer-->
	
	
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
    <script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>

    <script src="js/main.js"></script>

    <!-- Start Style Switcher -->
    <div class="switcher"></div>
    <!-- End Style Switcher -->

	
</body>
</html>
