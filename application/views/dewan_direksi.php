<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Header-->
<?php $this->load->view('layout/header') ?>
                
<!--End Header-->
        
        <section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>Date</span></h4>
                            </div>
                            <ul class="datepicker">
                                <li>
                                    
                                </li>
                                
                            </ul>
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Profil Direksi</span></h4>
                            </div>
                            <h3 align="left">Direktur Utama : Ir. A. Unggul Attas, MBA</h3>
                            <p><img align="left" src="<?php echo base_url(); ?>assets/images/dir01.jpg"></p>
                            <ol style="margin-left:188px">
                                <li> Direktur Utama PT Semen Tonasa sejak 1 Oktober 2012</li>
                                <li> Berkarir di PT Semen Tonasa, 1987-sampai sekarang</li>
                                <li> Koordinator Office of The CEO (OOTC) PT Semen Indonesia (Persero) Tbk. sejak 2009-2011</li>
                                <li> Pemasaran PT Semen Tonasa Juni 2011-Oktober 2012</li>
                                <li> Sarjana Teknik Elektro dari Universitas Hasanuddin tahun 1985</li>
                                <li> Master of Business Administration dari IBM Global tahun 1999</li>
                            </ol>
                            <h3 align="left">Direktur Produksi : Ir. Joko Sulistiyanto</h3>
                            <p><img align="left" src="<?php echo base_url(); ?>assets/images/dir02.jpg"></p>
                            <ol style="margin-left:188px">
                                <li>    Menjabat Direktur Produksi sejak April 2016</li>
                                <li>    Kepala Departemen Produksi Klinker I VO Semen Indonesia 2014 - 2016</li>
                                <li>    Kepala Departemen Produksi Klinker I PT Semen Indonesia 2013 </li>
                                <li>    Kepala Departemen Teknik PT Semen Indonesia 2011 - 2013</li>
                                <li>    Kepala Departemen Produksi Semen PT Semen Indonesia 2011</li>
                                <li>    Sarjana Teknik Mesin Institut Teknologi Sepuluh Nopember tahun 1999</li>
                            </ol>
                            <h3 align="left">Direktur Keuangan : Subhan, SE. Ak</h3>
                            <p><img align="left" src="<?php echo base_url(); ?>assets/images/dir03.jpg"></p>
                            <ol style="margin-left:188px">
                                <li> Direktur Keuangan PT Semen Tonasa sejak 1 Oktober 2012</li>
                                <li> Pernah berkarier di PT Rante Mario (Humpus Group) tahun 1996-1998</li>
                                <li> Memulai karier di PT Semen Tonasa sejak tahun 1998</li>
                                <li> Kepala Departemen Akuntansi PT Semen Tonasa sejak tahun 2006-2012</li>
                                <li> Sarjana Ekonomi Akuntansi dari Fakultas Ekonomi Jurusan Akuntansi Universitas Hasanuddin pada tahun 1997</li>
                                <li> Magister Manajemen dari Universitas Hasanuddin</li>
                            </ol>
                            <br><br>
                            <h3 align="left">Direktur Komersial : Ir. Tri Abdisatrijo</h3>
                            <p><img align="left" src="<?php echo base_url(); ?>assets/images/dir04.jpg"></p>
                            <ol style="margin-left:188px">
                                <li> Menjabat sebagai Direktur Komersial sejak 18 Juli 2014</li>
                                <li> Kepala Departemen Energi, Material dan Lingkungan PT Semen Indonesia (persero), Tbk. Pebruari 2014-Juli 2014</li>
                                <li>GM Project Specialist OOTC PT Semen Indonesia (persero), Tbk. 2013-2014</li>
                                <li> Kepala Departemen Produksi Semen PT Semen Tonasa 2009-2013</li>
                                <li>Berkarir di PT Semen Tonasa sejak 1992</li>
                                <li>Sarjana Teknik Kimia Institut Teknologi Sepuluh Nopember Surabaya tahun 1992</li>
                            </ol>
                        </div>
                        
                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>News</span></h4>
                            </div>
                            <p>Nunc et magna nisi. lore Aliquam at erat in lorem purus aliquet mollis. Fusce elementum velit vel dolor iaculis. </p>
                            <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <!--end wrapper-->

    <!--start footer-->
<?php $this->load->view('layout/footer') ?>
    <!--end footer-->
    
    
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
    <script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>

    <script src="js/main.js"></script>

    <!-- Start Style Switcher -->
    <div class="switcher"></div>
    <!-- End Style Switcher -->

    
</body>
</html>
