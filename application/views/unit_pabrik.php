<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Header-->
<?php $this->load->view('layout/header') ?>
                
<!--End Header-->
		
		<section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>Date</span></h4>
                            </div>
                            <ul class="datepicker">
                                <li>
                                    
                                </li>
                                
                            </ul>
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Profil Pabrik</span></h4>
                            </div>
                            <img src="<?php echo base_url(); ?>assets/images/tonasa_1.jpg" alt="Tonasa 1"  align="right"><h2>Tonasa I</h2>
                            <p>PT Semen Tonasa didirikan sesuai TAP MPRS No.II/MPRS/1960 tanggal 5 Desember 1960 dengan kepemilikan 100% pemerintah. Tonasa I beroperasi dengan kapasitas 120.000 ton per tahun. Tahun 1984, Tonasa I dihentikan operasionalnya karena alasan ekonomis </p>
                            <br><br><br>
                            <img src="<?php echo base_url(); ?>assets/images/tonasa_2.jpg" width="300" height="210" alt="Tonasa 2&3" style="margin-left:10px" align="right"><h2>Tonasa II</h2>
                            <p>PTahun 1980 Tonasa II beroperasi dengan kapasitas terpasang 510.000 ton per tahun. Tahun 1991 dilakukan optimalisasi sehingga kapasitas Tonasa II menjadi 590.000 ton pertahun. </p>
                            <h2>Tonasa III</h2>
                            <p>Tahun 1985 Tonasa III beroperasi dengan kapasitas terpasang 590.000 ton per tahun.</p>
                            <br><br><br>
                            <img src="<?php echo base_url(); ?>assets/images/tonasa_4.jpg" alt="Tonasa 4" style="margin-left:10px" align="right"><h2>Tonasa IV</h2>
                            <p>Tahun 1996 Tonasa IV beroperasi dengan kapasitas 2,3 Juta ton per tahun pada saat yang bersamaan beroperasi pula power plant 1 dengan kapasitas 2 x 25 MW </p>
                            <br><br><br><br><br>
                            <img src="<?php echo base_url(); ?>assets/images/tonasa_5.jpg" alt="Tonasa 5" style="margin-left:10px" align="right"><h2>Tonasa V</h2>
                            <p>Tonasa V beroperasi secara komersil sejak 1 Pebruari 2013.Pabrik Tonasa V memiliki kapasitas terpasang 2,5 juta ton per tahun. Pabrik Tonasa V dan Pembangkit Listrik 2 x 35 MW diresmikan oleh Presiden RI Susilo Bambang Yudhoyono pada tanggal 19 Februari 2014. </p>
                        </div>
                        
                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>News</span></h4>
                            </div>
                            <p>Nunc et magna nisi. lore Aliquam at erat in lorem purus aliquet mollis. Fusce elementum velit vel dolor iaculis. </p>
                            <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                            </div>
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Fasilitas Pendukung</span></h4>
                                <br><br>
                            </div>
                            <!-- <h2><p style="text-align: center;align:center">VISI</p></h2> -->
                            <!-- <img class="left_img img-thumbnail" src="<?php echo base_url(); ?>assets/images/pabrikok.jpg"> -->
                            <img src="<?php echo base_url(); ?>assets/images/pembangkit.jpg" alt="Tonasa 1"  align="right"><h2>Pembangkit Listrik BTG</h2>
                            <p>Empat unit pembangkit listrik tenaga uap atau Boiler Turbin Generator (BTG) Power Plant dengan kapasitas 2 X 25 MW dan 2 x 35 MW yang berlokasi di area Pelabuhan Biringkassi, Kabupaten Pangkep, sekitar 17 km dari lokasi pabrik. </p>
                            <br><br><br>
                            <img src="<?php echo base_url(); ?>assets/images/pelabuhan.jpg" alt="Tonasa 4" style="margin-left:10px" align="right"><h2>Pelabuhan Khusus Biringkassi</h2>
                            <p>Pelabuhan Biringkasi yang dapat disandari oleh kapal dengan muatan sampai 15.000 DWT berjarak 17 km dari lokasi pabrik. </p>
                            <br><br><br><br><br>
                            <img src="<?php echo base_url(); ?>assets/images/coal_unloading.jpg" alt="Tonasa 5" style="margin-left:10px" align="right"><h2>Coal Unloading</h2>
                            <p>Fasilitas Coal Unloading System yang berlokasi di area Biringkassi dengan kapasitas pembongkaran mencapai 1000 ton/jam. </p>
                        </div>
                        
                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading"></div>
            </div>
        </section>

 

	</section>
	<!--end wrapper-->

	<!--start footer-->
<?php $this->load->view('layout/footer') ?>
	<!--end footer-->
	
	
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
    <script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>

    <script src="js/main.js"></script>

    <!-- Start Style Switcher -->
    <div class="switcher"></div>
    <!-- End Style Switcher -->

	
</body>
</html>
