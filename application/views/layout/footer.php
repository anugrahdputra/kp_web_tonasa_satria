<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-7 col-md-4 col-lg-4">
                <div class="widget_title">
                    <h4><span>KANTOR PUSAT</span></h4>
                </div>
                <div class="widget_content">
                    <ul class="contact-details-alt">
                        <li></i> <p><strong>Alamat</strong>: Biringere, Pangkep, Sulawesi Selatan, 90651</p></li>
                        <li></i> <p><strong>Telephone</strong>: (0410) 312345</p></li>
                        <li></i> <p><strong>Fax</strong>: (0410) 310113</p></li>
                        <li></i> <p><strong>Website</strong>: www.sementonasa.co.id</p></li>
                        <li></i> <p><strong>Email</strong>: humas.st@semenindonesia.com</p></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-7 col-md-4 col-lg-4">
                <div class="widget_title">
                    <h4><span>KANTOR PERWAKILAN MAKASSAR</span></h4>
                </div>
                <div class="widget_content">
                    <ul class="contact-details-alt">
                        <li></i> <p><strong>Alamat</strong>: Jl. Khairil Anwar No.1, Makassar, Sulawesi Selatan, 90113</p></li>
                        <li></i> <p><strong>Telephone</strong>: (0411) 3621823</p></li>
                        <li></i> <p><strong>Fax</strong>: (0410) 3620649</p></li>
                        <li></i> <p><strong>Website</strong>: www.sementonasa.co.id</p></li>
                        <li></i> <p><strong>Email</strong>: humas.st@semenindonesia.com</p></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-7 col-md-4 col-lg-4">
                <div class="widget_title">
                    <h4><span>KANTOR PERWAKILAN JAKARTA</span></h4>
                </div>
                <div class="widget_content">
                    <ul class="contact-details-alt">
                        <li></i> <p><strong>Alamat</strong>: Graha Irama, Lantai 11, Blok X-1 Jl. HR. Rusuna Said, Kav 1-2, Jakarta, 12950</p></li>
                        <li></i> <p><strong>Telephone</strong>: (021) 5261161-4</p></li>
                        <li></i> <p><strong>Fax</strong>: (021) 5161160</p></li>
                        <li></i> <p><strong>Website</strong>: www.sementonasa.co.id</p></li>
                        <li></i> <p><strong>Email</strong>: humas.st@semenindonesia.com</p></li>
                    </ul>
                </div>
            </div>

            <!-- <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="widget_title">
                    <h4><span>Recent Posts</span></h4>
                </div>
                <div class="widget_content">
                    <ul class="links">
                        <li> <a href="#">Aenean commodo ligula eget dolor<span>November 07, 2015</span></a></li>
                        <li> <a href="#">Temporibus autem quibusdam <span>November 05, 2015</span></a></li>
                        <li> <a href="#">Debitis aut rerum saepe <span>November 03, 2015</span></a></li>
                        <li> <a href="#">Et voluptates repudiandae <span>November 02, 2015</span></a></li>
                    </ul>
                </div>
            </div> -->
            <!-- <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="widget_title">
                    <h4><span>Twitter</span></h4>

                </div>
                <div class="widget_content">
                    <ul class="tweet_list">
                        <li class="tweet_content item">
                            <p class="tweet_link"><a href="#">@jquery_rain </a> Lorem ipsum dolor et, consectetur adipiscing eli</p>
                            <span class="time">29 September 2015</span>
                        </li>
                        <li class="tweet_content item">
                            <p class="tweet_link"><a href="#">@jquery_rain </a> Lorem ipsum dolor et, consectetur adipiscing eli</p>
                            <span class="time">29 September 2015</span>
                        </li>
                        <li class="tweet_content item">
                            <p class="tweet_link"><a href="#">@jquery_rain </a> Lorem ipsum dolor et, consectetur adipiscing eli</p>
                            <span class="time">29 September 2015</span>
                        </li>
                    </ul>
                </div>
                <div class="widget_content">
                    <div class="tweet_go"></div>
                </div>
            </div> -->
            <!-- <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="widget_title">
                    <h4><span>Flickr Gallery</span></h4>
                </div>
                <div class="widget_content">
                    <div class="flickr">
                        <ul id="flickrFeed" class="flickr-feed"></ul>
                    </div>
                </div>
            </div> -->
        </div>
    </div>

    <section class="footer_bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <p class="copyright"><a target="_blank"  title="PT.Semen Tonasa"> &copy; 2017 Semen Tonasa Limited. All rights reserved
                <a data-toggle="modal" > <i class="icon-lock"></i></a>
            </div>

            <div class="col-sm-6 ">
                <div class="footer_social">
                    <ul class="footbot_social">
                        <li><a class="fb" href="#." data-placement="top" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="twtr" href="#." data-placement="top" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <!-- <li><a class="dribbble" href="#." data-placement="top" data-toggle="tooltip" title="Dribbble"><i class="fa fa-dribbble"></i></a></li>
                        <li><a class="skype" href="#." data-placement="top" data-toggle="tooltip" title="Skype"><i class="fa fa-skype"></i></a></li>
                        <li><a class="rss" href="#." data-placement="top" data-toggle="tooltip" title="RSS"><i class="fa fa-rss"></i></a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
</footer>