<!DOCTYPE html>
<head>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url();?>assets/js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.cookie.js"></script> <!-- jQuery cookie -->
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/styleswitch.js"></script> <!-- Style Colors Switcher -->
<!--
<script src="<?php echo base_url();?>assets/js/jquery.fractionslider.js" type="text/javascript" charset="utf-8"></script>
-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.smartmenus.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.smartmenus.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.jcarousel.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jflickrfeed.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/swipe.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flexslider-min.js"></script>

<script src="<?php echo base_url();?>assets/js/main.js"></script>
<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="hidden-xs col-lg-7 col-sm-5 top-info">
                <span><i class="fa fa-phone"></i>Phone: (0410) 312345</span>
                <span class="hidden-sm"><i class="fa fa-envelope"></i>Email: humas.st@semenindonesia.com</span>
            </div>
                <div class="col-lg-5 col-sm-7 top-info clearfix">
                    <ul>
                        <li><a class="my-facebook" href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a class="my-tweet" href=""><i class="fa fa-twitter"></i></a></li>
                        <!-- <li><a class="my-pint" href=""><i class="fa fa-pinterest"></i></a></li> -->
                        <!-- <li><a class="my-rss" href=""><i class="fa fa-rss"></i></a></li> -->
                        <!-- <li><a class="my-skype" href=""><i class="fa fa-skype"></i></a></li> -->
                        <!-- <li><a class="my-google" href=""><i class="fa fa-google-plus"></i></a></li> -->
                        <li>
                            <form class="search-bar">
                                <label for="search" class="search-label">
                                    <button class="search-button"><i class="fa fa-search"></i></button>
                                    <!-- Fix the break-row-bug-->
                                    <input type="text" id="search" class="search-input" />
                                </label>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
</div>

<div id="menu-bar">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-3 col-sm-3">
                        <div id="logo">
                             <h1><a href="index.html"><img src="<?php echo base_url();?>assets/images/logo.png"/></a></h1> 
                        </div>
                    </div> -->
                    
                    <div class="col-lg-12 col-sm-12 navbar navbar-default navbar-static-top container" role="navigation">
                        <!--  <div class="container">-->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="<?php echo base_url()?>"><span class="data-hover"data-hover="home">Beranda</span></a>
                                    <!-- <ul class="dropdown-menu">
                                        <li><a href="index_2.html">Home 2</a></li>
                                        <li><a href="index_3.html">Home 3</a></li>
                                        <li><a href="index_4.html">Home 4</a></li>
                                        <li><a href="index_5.html">Home 5</a></li>
                                    </ul> -->
                                </li>

                                <li><a href="#"><span class="data-hover" data-hover="PROFIL PERUSAHAAN">PROFIL PERUSAHAAN</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url()."profil_singkat/"; ?>">PROFIL SINGKAT</a></li>
                                        <li><a href="<?php echo base_url()."visi_misi/"; ?>">VISI MISI</a></li>
                                        <li><a href="<?php echo base_url()."unit_pabrik/"; ?>">UNIT PABRIK & FASILITAS PENDUKUNG</a></li>
                                        <li>
                                            <a href="#">STRUKTUR MANAJEMEN</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url()."dewan_komisaris/"; ?>">DEWAN KOMISARIS per 2012-2017</a></li>
                                                <li><a href="<?php echo base_url()."dewan_direksi/"; ?>">DIREKSI per 2012-2017</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="<?php echo base_url()."pemegang_saham/"; ?>">PEMEGANG SAHAM</a></li>
                                        <li>
                                            <a href="#">TATA KELOLA PERUSAHAAN</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url()."gcg/"; ?>">GCG</a></li>
                                                <li><a href="<?php echo base_url()."kode_etik/"; ?>">KODE ETIK</a></li>
                                                <li><a href="<?php echo base_url()."budaya_perusahaan/"; ?>">BUDAYA PERUSAHAAN</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="<?php echo base_url()."sdm/"; ?>">SDM</a></li>
                                        <li><a href="<?php echo base_url()."peristiwa_penting/"; ?>">PERISTIWA PENTING</a></li>
                                        <li><a href="<?php echo base_url()."sertifikasi_penghargaan/"; ?>">SERTIFIKASI DAN PENGHARGAAN</a></li>
                                        <li><a href="<?php echo base_url()."laporan_tahunan/"; ?>">LAPORAN TAHUNAN</a></li>
                                    </ul>
                                </li>

                                <li><a href="#" ><span class="data-hover" data-hover="PRODUKSI">PRODUKSI</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url()."jenis_produksi/"; ?>">JENIS PRODUKSI</a></li>
                                        <li><a href="<?php echo base_url()."proses_produksi/"; ?>">PROSES PRODUKSI</a></li>
                                    </ul>
                                </li>
                                <li><a href="#"><span class="data-hover" data-hover="LAYANAN PELANGGAN">LAYANAN PELANGGAN</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url()."pemasaran/"; ?>">PEMASARAN</a></li>
                                        <li><a href="<?php echo base_url()."unit_pengantongan/"; ?>">UNIT PENGANTONGAN SEMEN</a></li>
                                        <li><a href="<?php echo base_url()."jaringan_distribusi/"; ?>">JARINGAN DISTRIBUSI</a></li>
                                        <li><a href="#">SAHABAT TONASA</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url()."faq/"; ?>">FAQ</a></li>
                                                <li><a href="<?php echo base_url()."layanan/"; ?>">LAYANAN PELANGGAN</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li><a href="#"><span class="data-hover" data-hover="CSR">CSR</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url()."csr/"; ?>">CSR</a></li>
                                        <li><a href="#">KEPEDULIAN LINGKUNGAN</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url()."implementasi_afr/"; ?>">IMPLEMENTASI AFR</a></li>
                                                <li><a href="<?php echo base_url()."implementasi_cdm/"; ?>">IMPLEMENTASI CDM</a></li>
                                                <li><a href="<?php echo base_url()."video_csr/"; ?>">VIDEO CSR</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li><a href="#"> <span class="data-hover" data-hover="BERITA">BERITA</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">BERITA</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="views/berita/berita/2013.php">2013</a></li>
                                                <li><a href="views/berita/berita/2014.php">2014</a></li>
                                                <li><a href="views/berita/berita/2015.php">2015</a></li>
                                                <li><a href="views/berita/berita/2016.php">2016</a></li>
                                                <li><a href="views/berita/berita/2017.php">2017</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="views/berita/event.php">EVENT</a></li>
                                        <li><a href="views/berita/video.php">VIDEO</a></li>
                                    </ul>
                                </li>

                                <li><a href="#"> <span class="data-hover" data-hover="PUBLIKASI">PUBLIKASI</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url()."artikel_tja/"; ?>">ARTIKEL TJA</a></li>
                                    </ul>
                                </li>

                                <li><a href="<?php echo base_url()."rekrutmen/"; ?>"> <span class="data-hover" data-hover="REKRUTMEN">REKRUTMEN</span></a>
                                </li>

                                <li><a href="#"> <span class="data-hover" data-hover="AFILIASI">AFILIASI</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url()."ykst/"; ?>">YKST</a></li> 
                                    </ul>
                                </li>
                            </ul>
                        </div>

                                <!-- <li><a href="#"> <span class="data-hover" data-hover="KONTAK">KONTAK</span></a>
                                </li> -->

                            </ul>
                        </div>
                    <!-- </div> -->
                </div>
            </div>
            <!--/.container -->
        </div>
<head/>