<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Semen Tonasa</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/layout/wide.css" data-name="layout">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/switcher.css" media="screen" /> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Start Header-->
<?php $this->load->view('layout/header') ?>
                
<!--End Header-->
        
        <section class="content about">
            <div class="container">
                <div class="row sub_content">
                    <div class="who">

                        <!-- Bagian Kiri -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>Date</span></h4>
                            </div>
                            <ul class="datepicker">
                                <li>
                                    
                                </li>
                                
                            </ul>
                        </div>

                        <!-- Bagian Tengah -->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="dividerHeading">
                                <h4><span>Budaya Perusahaan</span></h4>
                            </div>
                                <!-- <img class="left_img img-thumbnail" src="<?php echo base_url(); ?>assets/images/pabrikok.jpg"> -->
                                <img class="left_img img-thumbnail" src="<?php echo base_url(); ?>assets/images/budaya_perusahaan.jpg" alt="Pemegang Saham" />
                                <p>Untuk mendukung kebijakan tata kelola perusahaan, Direksi telah menetapkan kebijakan penerapan GCG di perseroan yang mengatur mekanisme kerja antara Dewan Komisaris dengan Direksi, standar etika bisnis, kebijakan dan prosedur, pengendalian internal serta manajemen risiko. Sebagai kelanjutan atas kebijakan tersebut, telah ditetapkan etika bisnis perseroan yang berisi antara lain visi, misi perusahaan dan budaya korporasi, serta penerapan etika kerja yaitu mengamalkan 5 (lima) nilai-nilai GCG meliputi Transparancy, Accountability, Responsibility, Independency, dan Fairness, serta memiliki 6 (enam) perilaku utama yang telah menjelma menjadi Budaya Perusahaan yang digali dari sikap dan perilaku karyawan dan dikenal dengan istilah CHAMPS yaitu : </p>
                                <ol>
                                    <li>Ciptakan visi jelas yang sinergis untuk bersaing</li>
                                    <li>Hidupkan semangat belajar terus menerus</li>
                                    <li>Amalkan tugas dengan akuntabilitas tinggi</li>
                                    <li>Mantapkan usaha untuk penuhi harapan pelanggan</li>
                                    <li>Praktekkan etika bisnis dengan integritas tinggi</li>
                                    <li>Senantiasa tingkatkan kerjasama</li>
                                </ol>

                        </div>
                        
                        <!-- Bagian Kanan -->
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="dividerHeading">
                                <h4><span>News</span></h4>
                            </div>
                            <p>Nunc et magna nisi. lore Aliquam at erat in lorem purus aliquet mollis. Fusce elementum velit vel dolor iaculis. </p>
                            <ul class="progress-skill-bar">
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            HTML
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            CSS
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">90%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="90" role="progressbar" data-height="100">
                                            JavaScript
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">80%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="80" role="progressbar" data-height="100">
                                            MySQL
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="lable">70%</span>
                                    <div class="progress_skill">
                                        <div class="bar" data-value="70" role="progressbar" data-height="100">
                                            PHP
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <!--end wrapper-->

    <!--start footer-->
<?php $this->load->view('layout/footer') ?>
    <!--end footer-->
    
    
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
    <script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
    <script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/swipe.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed-min.js"></script>

    <script src="js/main.js"></script>

    <!-- Start Style Switcher -->
    <div class="switcher"></div>
    <!-- End Style Switcher -->

    
</body>
</html>
