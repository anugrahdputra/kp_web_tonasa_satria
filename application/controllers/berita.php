<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_model');
	}

	public function index()
	{
		//$this->load->model('model_products');
		$data['news'] = $this->news_model->allnews();
		$data['images'] = $this->news_model->allimages();
		$this->load->view('berita', $data);
	}	
}
?>