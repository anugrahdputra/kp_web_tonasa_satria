<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($year = null, $month = null)	{
		$prefs=array(
			'show_next_prev'=>TRUE,	
			'next_prev_url'=>base_url().'calendar/index/'
		);
			// if ($year==false){
			// 	$year=date('Y');
			// }
			// 	if ($month==false){
			// 		$month=date('m');
			// 	}
		$this->load->library('calendar',$prefs);
		echo $this->calendar->generate($year, $month);
		//$this->load->view('calendar');
	}
}
