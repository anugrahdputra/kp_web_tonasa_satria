<?php 
class Adm_login extends CI_Controller {
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('model_login');
	}

	public function index()	{
		if ($this->session->userdata('isLogin')==TRUE) {
			redirect('adm');
		}else {
			$this->load->view('admin/adm_login');	
		}
	}

	function do_login()	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		//print_r($username);
			$cek = $this->model_login->cek_user($username,$password);
				if (count($cek) ==1) {
					//echo "berhasil masuk";
					$this->session->set_userdata(array(
							'isLogin' 	=> TRUE, //set data telah login
							'username' 	=> $username, //set session user yang login
						));
					redirect('adm');
				}else 
				{
					//echo "gagal masuk";
					$this->session->set_flashdata('gagallogin', 'Maaf, username atau password yang dimasukkan salah');
					redirect('adm_login');
				}

	}
}
?>