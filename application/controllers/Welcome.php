<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('news_model');
	}	
	public function index($id_berita)
	{
		//$data['news'] = $this->model_news->GetNews();
		$data['news']=$this->news_model->findnews();
		//$data['image']=$this->news_model->findimages($id_berita);
		$this->load->view('index',$data);
	}

	/*public function editnews($id_berita){
		$data = $this->news_model->getberita($id_berita);
		$this->load->view('admin/adm_edit',array('data' => $data));
	}	*/
}