<?php 
class Adm_table extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('news_model');
	}	
	public function index()
	{
		if ($this->session->userdata('isLogin')== TRUE) {
			$data['news']=$this->news_model->allnews();
			$data['image']=$this->news_model->allimages();
			$this->load->view('admin/adm_table', $data);
		}else {
			redirect('adm_login');
		}			
	} 

	public function deletenews($id_berita){
		$this->news_model->deleteberita($id_berita);
		$path = $_SERVER['DOCUMENT_ROOT'].'/upload/';
		/*$files = glob($path.'*'); // get all file names
    		foreach($id_berita as $file){ // iterate files
      			if(is_file($file))
        			unlink($file); // delete file
        			//echo $file.'file deleted';
    		} */  

		redirect('adm_table'); // ini masih belom delet gambarnya yg di direktori ???
	}

	public function editnews($id_berita){
		$data = $this->news_model->getberita($id_berita);
		$this->load->view('admin/adm_edit',array('data' => $data));
	}

	public function submitedit(){
		$this->load->model('news_model');
		$id_berita       = $this->input->post('id_berita');
		$judul_berita 		= $this->input->post('judul_berita');
		$kategori_berita 	= $this->input->post('kategori_berita');
		$isi_berita 		= $this->input->post('isi_berita');
		$video_berita 		= $this->input->post('video_berita');
		$date 				= $this->input->post('date');
		//menyimpan ke database
		$this->news_model->updateberita($id_berita,$judul_berita,$kategori_berita,$isi_berita,$video_berita,$date);

		// get id berita
		$idberita = $this->news_model->getidberita($this->input->post('judul_berita'),$this->input->post('date'));
		$idberita = $idberita->id_berita;

		//$path = base_url()."upload/";
		
		//$this->upload_files($idberita);
		//redirect('adm_form');

		$config4['upload_path']   = './upload/';
        $config4['allowed_types'] = 'gif|jpg|png|ico';
        $this->load->library('upload',$config4);

        $filesCount = count($_FILES['images']['name']);
        $datanamafile = array();
        $jumlahfile = 0;
        for ($i=0; $i<$filesCount ; $i++) 
        { 
            $_FILES['images2']['name'] = $_FILES['images']['name'][$i];
            $_FILES['images2']['type'] = $_FILES['images']['type'][$i];
            $_FILES['images2']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
            $_FILES['images2']['error'] = $_FILES['images']['error'][$i];
            $_FILES['images2']['size'] = $_FILES['images']['size'][$i];

            $this->upload->initialize($config4);
            if($this->upload->do_upload('images2')){
                $upload_data4 = $this->upload->data();
                $namafile = $upload_data4['file_name'];
                $datanamafile[$i] = $namafile;
                $jumlahfile++;
            }else{
                // $error = array('error' => $this->upload->display_errors());
                // print_r($error);
            }
        }

        for ($j=0; $j < $jumlahfile ; $j++) { 
        	$this->news_model->masukkanfile($datanamafile[$j],$idberita);
        }
        redirect('adm_table');
	}
}
?>