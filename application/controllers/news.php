<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
	
	function __construct(){
		parent::__construct();
	}

	public function index(){
        $this->load->model('news_model');
        $berita = $this->news_model->getBeritaAll();
		$this->load->view('index',array('berita' => $berita));
        //print_r($berita);
	}


	public function submit(){
		/*$config4['upload_path'] = './upload/';
        $config4['allowed_types'] = 'gif|jpg|png|pdf';
            
        $this->load->library('upload', $config4);*/

		//input berita
		$this->load->model('news_model');
		$judul_berita 		= $this->input->post('judul_berita');
		$kategori_berita 	= $this->input->post('kategori_berita');
		$isi_berita 		= $this->input->post('isi_berita');
		$video_berita 		= $this->input->post('video_berita');
		$date 				= $this->input->post('date');
		//menyimpan ke database
		$this->news_model->inputberita($judul_berita,$kategori_berita,$isi_berita,$video_berita,$date);

		// get id berita
		$idberita = $this->news_model->getidberita($this->input->post('judul_berita'),$this->input->post('date'));
		$idberita = $idberita->id_berita;

		//$path = base_url()."upload/";
		
		//$this->upload_files($idberita);
		//redirect('adm_form');

		$config4['upload_path']   = './upload/';
        $config4['allowed_types'] = 'gif|jpg|png|ico';
        $this->load->library('upload',$config4);

        $filesCount = count($_FILES['images']['name']);
        $datanamafile = array();
        $jumlahfile = 0;
        for ($i=0; $i<$filesCount ; $i++) 
        { 
            $_FILES['images2']['name'] = $_FILES['images']['name'][$i];
            $_FILES['images2']['type'] = $_FILES['images']['type'][$i];
            $_FILES['images2']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
            $_FILES['images2']['error'] = $_FILES['images']['error'][$i];
            $_FILES['images2']['size'] = $_FILES['images']['size'][$i];

            $this->upload->initialize($config4);
            if($this->upload->do_upload('images2')){
                $upload_data4 = $this->upload->data();
                $namafile = $upload_data4['file_name'];
                $datanamafile[$i] = $namafile;
                $jumlahfile++;
            }else{
                // $error = array('error' => $this->upload->display_errors());
                // print_r($error);
            }
        }

        for ($j=0; $j < $jumlahfile ; $j++) { 
        	$this->news_model->masukkanfile($datanamafile[$j],$idberita);
        }
        redirect('adm_form');
        

        //print_r($datanamafile);

        // echo $idberita;
	}

	private function upload_files($idberita)
    {
    	$this->load->model('news_model');

    	$config4['upload_path']   = './upload/';
        $config4['allowed_types'] = 'gif|jpg|png|ico';
        $config4['max_size']       = 5; //MB
        //onfig4['max_width']      = 1024;
        //$config4['max_height']     = 768;
        $this->load->library('upload',$config4);

        $filesCount = count($_FILES['images']['name']);
        for ($i=0; $i<$filesCount ; $i++) 
        { 
            $_FILES['images2']['name'] = $_FILES['images']['name'][$i];
            $_FILES['images2']['type'] = $_FILES['images']['type'][$i];
            $_FILES['images2']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
            $_FILES['images2']['error'] = $_FILES['images']['error'][$i];
            $_FILES['images2']['size'] = $_FILES['images']['size'][$i];

            $this->upload->initialize($config4);
            if($this->upload->do_upload('images2')){
                $upload_data4 = $this->upload->data();
                $namafile = $upload_data4['file_name'];
                //$this->news_model->masukkanfile($namafile,$idberita);
            }
        }

        

        // if($this->upload->do_upload('userfile')){
        // 	//$token=$this->input->post('token_foto');
        // 	$nama=$this->upload->data('file_name');
        // 	$this->db->insert('images',array('foto_berita'=>$nama));
        // }else{
        //     $error = array('error' => $this->upload->display_errors());
        //     print_r($error);
        // }
    }

    
}


